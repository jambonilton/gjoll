package org.nps.gjoll;

import org.nps.gjoll.events.DataPublisher;
import org.nps.gjoll.updates.MutableDataSink;

public interface DataStore extends ReadOnlyStore, MutableDataSink, DataPublisher {

    default <T extends DTO> Store<T> forType(Class<T> type) {
        return new TypedStore<>(this, new Converter<DTO, T>() {
            @Override
            public T to(DTO in) {
                return in.asA(type);
            }
            @Override
            public DTO from(T out) {
                return out;
            }
        });
    }

}