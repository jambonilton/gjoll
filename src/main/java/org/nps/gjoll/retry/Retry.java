package org.nps.gjoll.retry;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

public interface Retry<T> {

	public static <T> Retry.Builder<T> wrap(Callable<T> callable) {
		return new Retry.Builder<>(callable);
	}

	static class Builder<T> {
		
		private static final int DEFAULT_TIMES = 5;
		private static final long DEFAULT_DELAY = 200L;
		
		final Callable<T> callable;
		Integer times = null;
		Delayer delayer = new ConstantDelayer(DEFAULT_DELAY, TimeUnit.MILLISECONDS);
		Predicate<Exception> retryOn = t -> true;

		public Builder(Callable<T> callable) {
			this.callable = callable;
		}

		public Builder<T> times(int times) {
			this.times = times;
			return this;
		}

		public Builder<T> delay(long delayInMilliseconds) {
			return delay(delayInMilliseconds, TimeUnit.MILLISECONDS);
		}

		public Builder<T> delay(long delay, TimeUnit timeUnit) {
			this.delayer = new ConstantDelayer(delay, timeUnit);
			return this;
		}

		public Builder<T> delays(long... delaysInMilliseconds) {
			return delays(TimeUnit.MILLISECONDS, delaysInMilliseconds);
		}

		public Builder<T> delays(TimeUnit timeUnit, long... delays) {
			this.delayer = new SerialDelayer(delays, timeUnit);
			if (times == null)
				times = delays.length;
			return this;
		}

		public Builder<T> on(Predicate<Exception> retryOn) {
			this.retryOn = retryOn;
			return this;
		}

		public final Builder<T> on(Class<? extends Exception> type) {
			this.retryOn = type::isInstance;
			return this;
		}

		@SafeVarargs
		public final Builder<T> on(Class<? extends Exception>... types) {
			this.retryOn = (e) -> false;
			for (Class<? extends Exception> type : types)
				this.retryOn = retryOn.or(type::isInstance);
			return this;
		}

		public Callable<T> build() {
			return () -> call();
		}

		public T call() throws Exception {
			final int times = this.times == null ? DEFAULT_TIMES : this.times;
			Exception last = new RuntimeException();
			for (int i = 0; i < times; i++) {
				try {
					return callable.call();
				} catch (Exception e) {
					if (!retryOn.test(e))
						throw e;
					last = e;
					delayer.delay();
				}
			}
			throw last;
		}
	}

}
