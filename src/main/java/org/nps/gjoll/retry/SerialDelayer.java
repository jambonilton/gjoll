package org.nps.gjoll.retry;

import java.util.concurrent.TimeUnit;

public class SerialDelayer implements Delayer {

	private long[] delay;
	private TimeUnit timeUnit;
	
	int index=0;
	
	public SerialDelayer(long[] delay, TimeUnit timeUnit) {
		this.delay = delay;
		this.timeUnit = timeUnit;
	}

	@Override
	public void delay() throws InterruptedException {
		Thread.sleep(timeUnit.toMillis(delay[index++]));
		index %= delay.length;
	}

}
