package org.nps.gjoll.retry;

import java.util.concurrent.TimeUnit;

public class ConstantDelayer implements Delayer {

	private long delay;
	private TimeUnit timeUnit;
	
	public ConstantDelayer(long delay, TimeUnit timeUnit) {
		this.delay = delay;
		this.timeUnit = timeUnit;
	}

	@Override
	public void delay() throws InterruptedException {
		Thread.sleep(timeUnit.toMillis(delay));
	}
	
}
