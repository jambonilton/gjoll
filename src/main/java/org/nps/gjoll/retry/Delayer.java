package org.nps.gjoll.retry;

public interface Delayer {
	
	void delay() throws InterruptedException;
	
}
