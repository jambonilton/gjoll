package org.nps.gjoll;

public interface Converter<I,O> {

    O to(I in);

    I from(O out);

}
