package org.nps.gjoll.tables;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Pair;

public class TableDescriptorImpl implements TableDescriptor {

    final List<ColumnDescriptor> columns;
    
    public TableDescriptorImpl(List<ColumnDescriptor> columns) {
        super();
        this.columns = columns;
    }

    @Override
    public Iterator<ColumnDescriptor> iterator() {
        return columns.iterator();
    }

    @Override
    public int size() {
        return columns.size();
    }

    @Override
    public ColumnDescriptor get(int index) {
        return columns.get(index);
    }

    @Override
    public Map<String, Integer> indices() {
        Map<String, Integer> indices = new HashMap<String,Integer>();
        for (int i=0; i < columns.size(); i++) 
            indices.put(columns.get(i).name(), i);
        return indices;
    }

    @Override
    public Stream<Pair<ColumnDescriptor, Object>> values(DTO obj) {
        return columns.stream().map((c)->new Pair<>(c, obj.get(c.name())));
    }

}
