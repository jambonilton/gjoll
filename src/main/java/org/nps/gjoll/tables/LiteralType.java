package org.nps.gjoll.tables;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.function.BiFunction;
import java.util.function.Function;

// TODO clean
public enum LiteralType {
    
    INT(LiteralType::readInt, LiteralType::writeInt), 
    FLOAT(LiteralType::readFloat, LiteralType::writeFloat), 
    STRING(LiteralType::readString, LiteralType::writeString), 
    BOOLEAN((b)->b[0]!=0, LiteralType::writeBoolean);

    Function<byte[], Object> fromBytes;
    BiFunction<Object, Integer, byte[]> toBytes;
    
    LiteralType(Function<byte[], Object> fromBytes,
            BiFunction<Object, Integer, byte[]> toBytes) {
        this.fromBytes = fromBytes;
        this.toBytes = toBytes;
    }

    public Object fromBytes(byte[] t) {
        return fromBytes.apply(t);
    }

    public byte[] toBytes(Object t, Integer u) {
        return toBytes.apply(t, u);
    }
    
    static Object readInt(byte[] b) {
        final int size = b.length;
        switch(size) {
        case 1: return b[0];
        case 2: return ByteBuffer.wrap(b).getShort();
        case 4: return ByteBuffer.wrap(b).getInt();
        case 8: return ByteBuffer.wrap(b).getLong();
        default: return new BigInteger(b);
        }
    }
    
    static byte[] writeInt(Object i, Integer size) {
        Number n = (Number) i;
        switch(size) {
        case 1: return ByteBuffer.allocate(size).put(n.byteValue()).array();
        case 2: return ByteBuffer.allocate(size).putShort(n.shortValue()).array();
        case 4: return ByteBuffer.allocate(size).putInt(n.intValue()).array();
        case 8: return ByteBuffer.allocate(size).putLong(n.longValue()).array();
        default: return ((BigInteger) i).toByteArray();
        }
    }
    
    static Object readFloat(byte[] b) {
        final int size = b.length;
        switch(size) {
        case 4: return ByteBuffer.wrap(b).getFloat();
        case 8: return ByteBuffer.wrap(b).getDouble();
        default: return new BigDecimal(new String(b));
        }
    }
    
    static byte[] writeFloat(Object i, Integer size) {
        Number n = (Number) i;
        if (i instanceof Float)
            return ByteBuffer.allocate(size).putFloat(n.floatValue()).array();
        else if (i instanceof Double)
            return ByteBuffer.allocate(size).putDouble(n.doubleValue()).array();
        else if (i instanceof BigDecimal) {
            return writeString(((BigDecimal) i).toPlainString(), size);
        }
        throw new IllegalArgumentException("Cannot process number with type \""+i.getClass()+"\"!");
    }
    
    private static String readString(final byte[] bytes) {
        if (bytes.length == 0)
            return "";
        int end;
        for (end = bytes.length-1; end >= 0 && bytes[end] == 0; end--)
            ;
        return new String(bytes, 0, end+1);
    }

    private static byte[] writeString(final Object str, Integer size) {
        return ByteBuffer.allocate(size).put(((String)str).getBytes()).array();
    }
    
    private static byte[] writeBoolean(final Object bool, Integer size) {
        if (bool instanceof Boolean)
            return ((Boolean) bool) ? new byte[]{0x01} : new byte[]{0x00};
        return bool != null ? new byte[]{0x01} : new byte[]{0x00};
    }
    
}
