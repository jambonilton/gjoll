package org.nps.gjoll.tables;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.nps.gjoll.io.SerializationException;

public class ColumnDescriptorImpl implements ColumnDescriptor {

    final ThreadLocal<byte[]> bytes;
    final String name;
    final LiteralType type;
    final int size;
    
    public ColumnDescriptorImpl(String name, LiteralType type, int size) {
        this.name = name;
        this.type = type;
        this.size = size;
        this.bytes = new ThreadLocal<byte[]>() {
            @Override
            protected byte[] initialValue() {
                return new byte[size];
            }
        };
    }

    @Override
    public void write(Object t, OutputStream out) {
        try {
            out.write(type.toBytes(t, size));
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }

    @Override
    public Object read(InputStream in) throws IOException {
        in.read(bytes.get());
        return type.fromBytes(bytes.get());
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public LiteralType type() {
        return type;
    }

    @Override
    public int size() {
        return size;
    }

}
