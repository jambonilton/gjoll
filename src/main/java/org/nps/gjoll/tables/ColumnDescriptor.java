package org.nps.gjoll.tables;

import org.nps.gjoll.io.Format;

public interface ColumnDescriptor extends Format<Object> {
    String name();
    LiteralType type();
    int size();
}
