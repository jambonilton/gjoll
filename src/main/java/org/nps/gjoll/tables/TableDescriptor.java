package org.nps.gjoll.tables;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Pair;

public interface TableDescriptor extends Iterable<ColumnDescriptor> {

    int size();
    
    ColumnDescriptor get(int index);
    
    Map<String, Integer> indices();
    
    Stream<Pair<ColumnDescriptor, Object>> values(DTO obj);
    
    public static class Builder {
        
        private final List<ColumnDescriptor> columns = new ArrayList<ColumnDescriptor>();
        
        public Builder column(String name, LiteralType type, int size) {
            columns.add(new ColumnDescriptorImpl(name, type, size));
            return this;
        }
        
        public TableDescriptor build() {
            return new TableDescriptorImpl(columns);
        }
        
    }
    
}
