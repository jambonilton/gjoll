package org.nps.gjoll.tables;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.SerializationException;

public class BasicColumnDescriptorFormat implements Format<ColumnDescriptor> {

    private static final int separator = ':';
    
    @Override
    public void write(ColumnDescriptor c, OutputStream out) {
        try {
            out.write(c.name().getBytes());
            out.write(separator);
            out.write(c.type().ordinal());
            out.write(c.size());
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }

    @Override
    public ColumnDescriptor read(InputStream in) throws IOException {
        final StringBuilder sb = new StringBuilder();
        int ch = in.read();
        while (ch != separator)
            sb.append((char) ch);
        return new ColumnDescriptorImpl(sb.toString(), LiteralType.values()[in.read()], in.read());
    }

}
