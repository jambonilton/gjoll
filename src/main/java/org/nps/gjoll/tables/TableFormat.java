package org.nps.gjoll.tables;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.nps.gjoll.DTO;
import org.nps.gjoll.io.QueryableFormat;
import org.nps.gjoll.io.SerializationException;
import org.nps.gjoll.std.ArrayDTO;

/**
 * Encodes objects in static-length blocks, as per the specification of the 
 * supplied table descriptor.
 */
public class TableFormat implements QueryableFormat {

    private static final int HEADER = '$';
    
    final TableDescriptor tableDescriptor;
    final Map<String, Integer> fieldIndex;
    
    public TableFormat(TableDescriptor tableDescriptor) {
        this.tableDescriptor = tableDescriptor;
        this.fieldIndex = tableDescriptor.indices();
    }

    @Override
    public void write(DTO obj, OutputStream out) {
        try {
            out.write(HEADER);
            tableDescriptor.values(obj).forEach((t)->t.getKey().write(t.getValue(), out));
            out.flush();
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }

    @Override
    public DTO read(InputStream in) throws IOException {
        final int head = in.read();
        if (head == -1)
            return null;
        final Object[] values = new Object[tableDescriptor.size()];
        int i=0;
        for (ColumnDescriptor c : tableDescriptor)
            values[i++] = c.read(in);
        return new ArrayDTO(fieldIndex, values);
    }
    
}
