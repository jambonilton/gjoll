package org.nps.gjoll;

public interface DataStoreRegistry {
    DataStore get(String name);
    void set(String name, DataStore store);

    static DataStoreRegistry of(final String name, final DataStore store) {
        return new DataStoreRegistry() {
            @Override
            public DataStore get(String k) {
                if (k.equals(name))
                    return store;
                throw new DataStoreNotFoundException(k);
            }

            @Override
            public void set(String name, DataStore store) {
                throw new UnsupportedOperationException();
            }
        };
    }

    class DataStoreNotFoundException extends RuntimeException {
        public DataStoreNotFoundException(String name) {
            super(String.format("Data store with name \"%s\" not found!", name));
        }
    }
}
