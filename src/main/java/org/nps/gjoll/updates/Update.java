package org.nps.gjoll.updates;

import org.nps.gjoll.Gjoll;
import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Pair;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.ValueExpression;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nps.gjoll.streams.IteratorUtils.transform;

public interface Update extends Iterable<Pair<String, ValueExpression>>, Function<DTO, DTO> {
    
    Query where();
    
    static Builder set(String field, Object value) {
        return new Builder(field, value);
    }
    
    static Builder from(Iterable<Pair<String, ValueExpression>> values) {
        return new Builder(values);
    }

    default Iterable<Pair<String, Object>> assignments(DTO obj) {
        return transform(this, (p)->new Pair<>(p.getKey(), p.getValue().getValue(obj)));
    }

    class Builder {
        
        final List<Pair<String, ValueExpression>> assignments;
        
        public Builder(Iterable<Pair<String, ValueExpression>> assignments) {
            this.assignments = new ArrayList<>();
            for (Pair<String,ValueExpression> assignment : assignments)
                this.assignments.add(assignment);
        }

        Builder(String field, Object value) {
            assignments = new ArrayList<>();
            set(field, value);
        }

        public Builder set(String field, Object value) {
            final ValueExpression expression = value instanceof ValueExpression
                    ? (ValueExpression) value : new UnboundValueExpression(value);
            this.assignments.add(new Pair<>(field, expression));
            return this;
        }
        
        public Update where(String query) {
            return where(Gjoll.getQueryFormat().read(query));
        }
        
        public Update where(Query query) {
            return new UpdateImpl(assignments, query);
        }
        
        public Update build() {
            return where(Query.alwaysTrue());
        }
        
    }
    
    class UpdateImpl implements Update {

        final List<Pair<String, ValueExpression>> assignments;
        final Query where;
        
        public UpdateImpl(List<Pair<String, ValueExpression>> assignments, Query where) {
            this.assignments = assignments;
            this.where = where;
        }

        @Override
        public Iterator<Pair<String, ValueExpression>> iterator() {
            return assignments.iterator();
        }

        @Override
        public DTO apply(DTO t) {
            return new OverrideDTO(t, assignments(t));
        }

        @Override
        public Query where() {
            return where;
        }

        @Override
        public String toString() {
            return assignments.stream().map((p)->p.getKey()+"←"+p.getValue()).collect(Collectors.joining(", "));
        }
        
    }

}
