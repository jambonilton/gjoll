package org.nps.gjoll.updates;

import org.nps.gjoll.DTO;
import org.nps.gjoll.query.ValueExpression;

public class UnboundValueExpression implements ValueExpression {

    final Object value;

    public UnboundValueExpression(Object value) {
        this.value = value;
    }

    @Override
    public Object getValue(DTO context) {
        return value;
    }

}
