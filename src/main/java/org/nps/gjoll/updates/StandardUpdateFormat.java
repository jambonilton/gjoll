package org.nps.gjoll.updates;

import org.nps.gjoll.Gjoll;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.PeekingInputStream;
import org.nps.gjoll.io.SerializationException;
import org.nps.gjoll.io.format.JsonFormat;
import org.nps.gjoll.query.Pair;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.ValueExpression;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StandardUpdateFormat implements Format<Update> {

    final Format<Query> queryFormat;

    public StandardUpdateFormat() {
        this(Gjoll.getQueryFormat());
    }

    public StandardUpdateFormat(Format<Query> queryFormat) {
        this.queryFormat = queryFormat;
    }

    @Override
    public void write(Update update, OutputStream out) {
        writeUpdate(update, out);
        queryFormat.write(update.where(), out);
    }

    public void writeUpdate(Update update, OutputStream out) {
        try {
            final Iterator<Pair<String, ValueExpression>> pairs = update.iterator();
            if (!pairs.hasNext()) {
                out.write('\n');
                return;
            }
            writePair(pairs.next(), out);
            while (pairs.hasNext()) {
                writePair(pairs.next(), out);
                out.write(',');
            }
            out.write('\n');
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }

    private void writePair(Pair<String, ValueExpression> pair, OutputStream out) throws IOException {
        out.write((pair.getKey()+"←").getBytes());
        JsonFormat.writeValue(out, pair.getValue());
    }

    @Override
    public Update read(InputStream input) throws IOException {
        final PeekingInputStream in = new PeekingInputStream(input);
        final List<Pair<String, ValueExpression>> assignments = new ArrayList<>();
        int ch = in.peek();
        while (ch != -1 && ch != '\n') {
            final String field = in.readTo((c) -> c == '←');
            final ValueExpression value = new UnboundValueExpression(JsonFormat.readValue(in));
            assignments.add(new Pair<>(field, value));
            if ((ch = in.read()) != ',')
                break;
        }
        return Update.from(assignments).where(queryFormat.read(in));
    }

}
