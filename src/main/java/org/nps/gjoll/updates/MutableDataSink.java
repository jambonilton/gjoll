package org.nps.gjoll.updates;

import org.nps.gjoll.DataSink;

public interface MutableDataSink extends DataSink {

    void apply(Update update);
    
}
