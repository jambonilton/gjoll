package org.nps.gjoll.updates;

import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Pair;

import java.util.*;
import java.util.stream.Collectors;

import static org.nps.gjoll.streams.IteratorUtils.*;

// TODO optimize
public class OverrideDTO implements DTO {

    private static final long serialVersionUID = 1L;

    final DTO base;
    final Map<String, Object> values;

    public OverrideDTO(DTO base, Iterable<? extends Map.Entry<String, Object>> assignments) {
        this.base = base;
        this.values = new HashMap<>();
        for (Map.Entry<String, Object> e : assignments)
            this.values.put(e.getKey(), e.getValue());
    }

    @Override
    public Iterator<Pair<String, Object>> iterator() {
        return concat(filter(base.iterator(), (p)->values.containsKey(p.getKey())), transform(values.entrySet().iterator(), (e)->new Pair<>(e.getKey(), e.getValue())));
    }

    @Override
    public Object get(String field) {
        if (values.containsKey(field))
            return values.get(field);
        return base.get(field);
    }

    @Override
    public Collection<String> fields() {
        final Collection<String> fields = new TreeSet<>();
        fields.addAll(base.fields());
        fields.addAll(values.keySet());
        return fields;
    }

    @Override
    public Collection<Object> values() {
        return fields().stream().map(this::get).collect(Collectors.toList());
    }

    @Override
    public int size() {
        return base.size();
    }
}
