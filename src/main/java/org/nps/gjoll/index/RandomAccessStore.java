package org.nps.gjoll.index;

import org.nps.gjoll.DTO;
import org.nps.gjoll.ReadOnlyStore;
import org.nps.gjoll.query.Query;

import java.util.stream.LongStream;

public interface RandomAccessStore extends ReadOnlyStore, LongIndex<DTO> {
    
    LongStream getIndices(Query q);
    
}
