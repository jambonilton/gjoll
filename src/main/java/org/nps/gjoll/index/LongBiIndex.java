package org.nps.gjoll.index;

public interface LongBiIndex<E> extends LongIndex<E> {

    long get(E value);

}
