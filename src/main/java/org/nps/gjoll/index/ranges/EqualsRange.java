package org.nps.gjoll.index.ranges;

import java.util.Collections;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;

import org.nps.gjoll.query.Pair;

public class EqualsRange<T extends Comparable<T>> implements Range<T> {

    final T value;
    
    public EqualsRange(T value) {
        this.value = value;
    }

    @Override
    public boolean test(T t) {
        if (value == null)
            return t == null;
        return value.equals(t);
    }

    @Override
    public Bound<T> lowerBound() {
        return Bound.of(value);
    }

    @Override
    public Bound<T> upperBound() {
        return Bound.of(value);
    }

    @Override
    public boolean contains(Range<T> range) {
        return false;
    }

    @Override
    public Range<T> and(Range<T> range) {
        return range.contains(this) ? this : Range.empty();
    }

    @Override
    public boolean isContinuous() {
        return true;
    }

    @Override
    public Range<T> or(Range<T> range) {
        if (range.contains(this))
            return range;
        return new SparseRange<>(this, range);
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public <V> Set<Entry<T, V>> get(NavigableMap<T, V> map) {
        return Collections.singleton(new Pair<T,V>(value, map.get(value)));
    }

}
