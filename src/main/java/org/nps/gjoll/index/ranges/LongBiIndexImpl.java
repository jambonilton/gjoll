package org.nps.gjoll.index.ranges;

import com.carrotsearch.hppc.ObjectLongMap;
import com.carrotsearch.hppc.ObjectLongOpenHashMap;
import org.nps.gjoll.index.LongBiIndex;
import org.nps.gjoll.index.LongIndex;

public class LongBiIndexImpl<E> implements LongBiIndex<E> {

    final LongIndex<E> base;
    final ObjectLongMap<E> map;

    public LongBiIndexImpl(LongIndex<E> base) {
        this(base, new ObjectLongOpenHashMap<>());
    }

    public LongBiIndexImpl(LongIndex<E> base, ObjectLongOpenHashMap<E> map) {
        this.base = base;
        this.map = map;
    }

    @Override
    public long get(E e) {
        if (!map.containsKey(e)) {
            final long key = base.addAndLocate(e);
            map.put(e, key);
            return key;
        }
        return map.get(e);
    }

    @Override
    public E get(long index) {
        return base.get(index);
    }

    @Override
    public long addAndLocate(E e) {
        return base.addAndLocate(e);
    }
}
