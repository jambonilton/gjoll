package org.nps.gjoll.index.ranges;

import java.util.Collection;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import org.nps.gjoll.query.BooleanFormula;
import org.nps.gjoll.query.Comparator;
import org.nps.gjoll.query.Comparison;
import org.nps.gjoll.reflect.Reflect;

public interface Range<T extends Comparable<T>> extends Predicate<T>, BooleanFormula<Range<T>> {

    Bound<T> lowerBound();

    Bound<T> upperBound();
    
    boolean contains(Range<T> range);
    
    boolean isContinuous();
    
    boolean isNull();
    
    <V> Set<Entry<T, V>> get(NavigableMap<T, V> map);
    
    static <T extends Comparable<T>> Bound<T> min(Bound<T> a, Bound<T> b) {
        if (!a.isSet() && !b.isSet())
            return Bound.empty();
        else if (a.isSet() && !b.isSet())
            return a;
        else if (b.isSet() && !a.isSet())
            return b;
        return a.get().compareTo(b.get()) < 0 ? a : b;
    }

    static <T extends Comparable<T>> Bound<T> max(Bound<T> a, Bound<T> b) {
        if (!a.isSet() && !b.isSet())
            return Bound.empty();
        else if (a.isSet() && !b.isSet())
            return a;
        else if (b.isSet() && !a.isSet())
            return b;
        return a.get().compareTo(b.get()) > 0 ? a : b;
    }
    
    static <T extends Comparable<T>> Optional<Range<T>> fromComparison(Comparison c) {
        final Comparator op = c.comparator();
        switch (op) {
            case equals:
                return Optional.of(new EqualsRange<>(Reflect.<T>cast(c.getValue())));
            case isIn:
                return Optional.of(new InSetRange<>(Reflect.<Collection<T>>cast(c.getValue())));
            case lessThan:
                return Optional.of(new EnclosedRange<>(Bound.empty(), Bound.of(Reflect.<T>cast(c.getValue()), false)));
            case lessThanOrEqual:
                return Optional.of(new EnclosedRange<>(Bound.empty(), Bound.of(Reflect.<T>cast(c.getValue()))));
            case greaterThan:
                return Optional.of(new EnclosedRange<>(Bound.of(Reflect.<T>cast(c.getValue()), false), Bound.empty()));
            case greaterThanOrEqual:
                return Optional.of(new EnclosedRange<>(Bound.of(Reflect.<T>cast(c.getValue())), Bound.empty()));
            case contains:
            default:
                break;
        }
        return Optional.empty();
    }
    
    static <T extends Comparable<T>> Range<T> empty() {
        return new EmptyRange<>();
    }
    
    static <T extends Comparable<T>> Range<T> all() {
        return new GlobalRange<>();
    }

}
