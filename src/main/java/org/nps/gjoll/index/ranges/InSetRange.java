package org.nps.gjoll.index.ranges;

import java.util.Collection;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.nps.gjoll.query.Pair;

public class InSetRange<T extends Comparable<T>> implements Range<T> {

    final SortedSet<T> values;
    
    public InSetRange(Collection<T> values) {
        this.values = new TreeSet<>(values);
    }

    @Override
    public boolean test(T t) {
        return values.contains(t);
    }

    @Override
    public Range<T> and(Range<T> range) {
        return range.contains(this) ? this : Range.empty();
    }

    @Override
    public Range<T> or(Range<T> range) {
        if (range.contains(this))
            return range;
        return new SparseRange<>(this, range);
    }

    @Override
    public Bound<T> lowerBound() {
        return new Bound<T>(values.first(), true);
    }

    @Override
    public Bound<T> upperBound() {
        return new Bound<T>(values.last(), true);
    }

    @Override
    public boolean contains(Range<T> range) {
        return false;
    }

    @Override
    public boolean isContinuous() {
        return false;
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public <V> Set<Entry<T, V>> get(NavigableMap<T, V> map) {
        return values.stream().map((v)->new Pair<T,V>(v, map.get(v))).collect(Collectors.toSet());
    }

}
