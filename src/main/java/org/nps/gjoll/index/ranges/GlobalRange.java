package org.nps.gjoll.index.ranges;

import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;

public class GlobalRange<T extends Comparable<T>> implements Range<T> {

    @Override
    public boolean test(T t) {
        return true;
    }

    @Override
    public Range<T> and(Range<T> other) {
        return other;
    }

    @Override
    public Range<T> or(Range<T> other) {
        return this;
    }

    @Override
    public Bound<T> lowerBound() {
        return Bound.empty();
    }

    @Override
    public Bound<T> upperBound() {
        return Bound.empty();
    }

    @Override
    public boolean contains(Range<T> range) {
        return true;
    }

    @Override
    public boolean isContinuous() {
        return true;
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public <V> Set<Entry<T, V>> get(NavigableMap<T, V> map) {
        return map.entrySet();
    }

}
