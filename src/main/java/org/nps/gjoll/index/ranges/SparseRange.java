package org.nps.gjoll.index.ranges;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Result of OR'd ranges.  The range will have many sub-ranges where values can fall into.
 */
public class SparseRange<T extends Comparable<T>> implements Range<T> {

    Collection<Range<T>> children;

    @SafeVarargs
    public SparseRange(Range<T>... children) {
        this.children = new ArrayList<>();
        for (Range<T> child : children)
            this.children.add(child);
    }
    
    public SparseRange(Collection<Range<T>> children) {
        this.children = children;
    }

    @Override
    public Bound<T> lowerBound() {
        final Optional<Bound<T>> min = children.stream().map(Range::lowerBound).reduce(Range::min);
        return min.isPresent() ? min.get() : Bound.empty();
    }

    @Override
    public Bound<T> upperBound() {
        final Optional<Bound<T>> max = children.stream().map(Range::upperBound).reduce(Range::max);
        return max.isPresent() ? max.get() : Bound.empty();
    }

    @Override
    public boolean test(T t) {
        for (Range<T> r : children)
            if (r.test(t))
                return true;
        return false;
    }

    @Override
    public boolean contains(Range<T> range) {
        for (Range<T> r : children)
            if (r.contains(range))
                return true;
        return false;
    }

    @Override
    public Range<T> or(Range<T> range) {
        if (this.contains(range))
            return this;
        else if (range.contains(this))
            return range;
        this.children.add(range);
        return this;
    }

    @Override
    public Range<T> and(Range<T> range) {
        children = children.stream().map((c)->c.and(range))
                                    .filter((c)->!c.isNull())
                                    .collect(Collectors.toList());
        return children.isEmpty() ? Range.empty() : this;
    }

    @Override
    public boolean isContinuous() {
        return false;
    }

    @Override
    public boolean isNull() {
        return children.isEmpty();
    }

    @Override
    public <V> Set<Entry<T, V>> get(NavigableMap<T, V> map) {
        final Set<Entry<T,V>> entries = new HashSet<>();
        for (Range<T> r : children)
            entries.addAll(r.get(map));
        return entries;
    }

}
