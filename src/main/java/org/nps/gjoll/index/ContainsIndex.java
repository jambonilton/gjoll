package org.nps.gjoll.index;

import com.carrotsearch.hppc.LongOpenHashSet;
import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Comparison;
import org.nps.gjoll.query.Conjunction;
import org.nps.gjoll.query.Disjunction;
import org.nps.gjoll.query.Query;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.LongStream;
import java.util.stream.StreamSupport;

/**
 * For use in text searching, and one-to-many relationships.
 */
public class ContainsIndex implements Index {

    final String field;
    final Itemizer itemizer;
    final Map<Object, LongOpenHashSet> map; // TODO is LongHashSet too fat!

    public ContainsIndex(String field, Itemizer itemizer) {
        this.field = field;
        this.itemizer = itemizer;
        this.map = new HashMap<>();
    }

    @Override
    public IndexResult get(Query query) {
        final ContainsResult result = toContainsQuery(query);
        return new IndexResult(result.getDiff(), result.asStream());
    }

    private ContainsResult toContainsQuery(Query query) {
        if (query instanceof Conjunction) {
            final ContainsResult cr = new ContainsResult();
            for (Query sub : query)
                cr.and(toContainsQuery(sub));
            return cr;
        } else if (query instanceof Disjunction) {
            final ContainsResult cr = new ContainsResult();
            for (Query sub : query)
                cr.or(toContainsQuery(sub));
            return cr;
        } else if (query instanceof Comparison && ((Comparison) query).getKey().equals(field)) {
            final Optional<LongOpenHashSet> indices = get((Comparison) query);
            if (indices.isPresent())
                return new ContainsResult(indices.get());
        }
        return new ContainsResult(query);
    }

    private Optional<LongOpenHashSet> get(Comparison clause) {
        if (!field.equals(clause.getKey()))
            return null;
        switch (clause.comparator()) {
            case contains:
            case equals:
                return Optional.of(map.get(clause.getValue()));
            default:
                return null;
        }
    }

    @Override
    public void add(DTO item, long l) {
        itemizer.items(item.get(field)).forEach((k) -> addToMap(k, l));
    }

    private void addToMap(Object key, long location) {
        LongOpenHashSet list = map.get(key);
        if (list == null) {
            list = new LongOpenHashSet();
            map.put(key, list);
        }
        list.add(location);
    }

    private class ContainsResult {
        Query diff;
        LongOpenHashSet indices;

        ContainsResult() {
        }

        ContainsResult(LongOpenHashSet indices) {
            this.indices = indices;
        }

        ContainsResult(Query diff) {
            this.diff = diff;
        }

        // TODO cleanup
        ContainsResult or(ContainsResult result) {
            if (this.hasDiff()) {
                if (result.hasDiff())
                    diff = diff.or(result.diff);
            } else
                diff = result.diff;
            if (this.hasIndices()) {
                if (result.hasDiff())
                    indices.addAll(result.indices);
            } else
                indices = result.indices;
            return this;
        }
        ContainsResult and(ContainsResult result) {
            if (this.hasDiff()) {
                if (result.hasDiff())
                    diff = diff.and(result.diff);
            } else
                diff = result.diff;
            if (this.hasIndices()) {
                if (result.hasIndices())
                    indices.retainAll(result.indices);
            } else
                indices = result.indices;
            return this;
        }

        boolean hasIndices() {
            return indices != null;
        }

        boolean hasDiff() {
            return diff != null;
        }

        public LongStream asStream() {
            // TODO bad performance on default.
            if (indices == null) {
                final LongOpenHashSet allIds = new LongOpenHashSet();
                for (LongOpenHashSet set : map.values())
                    allIds.addAll(set);
                return StreamSupport.stream(allIds.spliterator(), false).mapToLong((c)->c.value);
            }
            return StreamSupport.stream(indices.spliterator(), false).mapToLong((c)->c.value);
        }

        public Query getDiff() {
            if (diff == null)
                return Query.alwaysTrue();
            return diff;
        }
    }
}
