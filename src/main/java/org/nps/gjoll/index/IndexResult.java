package org.nps.gjoll.index;

import java.util.stream.LongStream;

import org.nps.gjoll.query.Query;

public class IndexResult {

    final Query remainingClauses;
    final LongStream results;
    
    public IndexResult(Query remainingClauses, LongStream results) {
        this.remainingClauses = remainingClauses;
        this.results = results;
    }

    public Query diff() {
        return remainingClauses;
    }

    public LongStream stream() {
        return results;
    }
    
}
