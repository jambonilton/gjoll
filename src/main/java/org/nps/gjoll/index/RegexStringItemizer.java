package org.nps.gjoll.index;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class RegexStringItemizer implements Itemizer {

    final Pattern pattern;

    public RegexStringItemizer() {
        this("[^\\w]+");
    }
    
    public RegexStringItemizer(String regex) {
        this(Pattern.compile(regex));
    }
    
    public RegexStringItemizer(Pattern pattern) {
        this.pattern = pattern;
    }

    @Override
    public Stream<Object> items(Object obj) {
        final TokenizerIterator iter = new TokenizerIterator(String.valueOf(obj));
        final Spliterator<Object> spliter = Spliterators.spliteratorUnknownSize(iter, Spliterator.ORDERED);
        return StreamSupport.stream(spliter, false);
    }
    
    class TokenizerIterator implements Iterator<Object> {

        final Matcher matcher;
        final String str;
        int start, end=-1;
        
        public TokenizerIterator(String str) {
            this.matcher = pattern.matcher(str);
            this.str = str;
            advance();
        }

        private void advance() {
            start = end+1;
            if (matcher.find())
                end = matcher.start();
            else if (end != str.length())
                end = str.length();
            else
                end = -1;
        }

        @Override
        public boolean hasNext() {
            return end != -1;
        }

        @Override
        public Object next() {
            final String result = str.substring(start, end);
            advance();
            return result;
        }
        
    }

}
