package org.nps.gjoll.index;

public interface LongIndex<E> {

    E get(long index);

    long addAndLocate(E q);

}
