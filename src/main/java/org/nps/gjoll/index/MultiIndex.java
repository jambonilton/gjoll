package org.nps.gjoll.index;

import com.carrotsearch.hppc.LongOpenHashSet;
import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Query;

import java.util.List;
import java.util.stream.StreamSupport;

public class MultiIndex implements Index {

	final List<Index> indices;

	public MultiIndex(List<Index> indices) {
        this.indices = indices;
	}

	@Override
	public IndexResult get(Query query) {
        LongOpenHashSet set = null; // TODO use merged, sorted stream for better performance.
        Query diff = query;
        IndexResult result = null;
		for (Index index : indices) {
            result = index.get(diff);
            if (queryWasAccepted(diff, result)) {
                if (set == null)
                    set = toHashSet(result);
                else
                    set.retainAll(toHashSet(result));
                diff = result.diff();
            }
		}
        if (set == null)
            return new IndexResult(query, result.stream());
        return new IndexResult(diff, StreamSupport.stream(set.spliterator(), false).mapToLong((c) -> c.value));
	}

    private boolean queryWasAccepted(Query query, IndexResult result) {
        return !query.equals(result.diff());
    }

    private LongOpenHashSet toHashSet(IndexResult result) {
        final LongOpenHashSet subset = new LongOpenHashSet();
        result.stream().forEach(subset::add);
        return subset;
    }

    @Override
	public void add(DTO item, long location) {
		for (Index index : indices)
            index.add(item, location);
	}

}
