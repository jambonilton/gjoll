package org.nps.gjoll.index;

import java.util.ArrayList;
import java.util.List;

public class ListLongIndex<E> implements LongIndex<E> {

    final List<E> elements;

    public ListLongIndex() {
        this(new ArrayList<>());
    }

    public ListLongIndex(List<E> elements) {
        this.elements = elements;
    }

    @Override
    public E get(long index) {
        return elements.get((int) index);
    }

    @Override
    public synchronized long addAndLocate(E e) {
        final long location = elements.size();
        elements.add(e);
        return location;
    }

}
