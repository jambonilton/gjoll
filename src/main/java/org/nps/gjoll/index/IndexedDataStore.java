package org.nps.gjoll.index;

import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Query;

public class IndexedDataStore implements RandomAccessStore {

    final Index index;
    final RandomAccessStore base;
    
    public IndexedDataStore(Index index, RandomAccessStore base) {
        this.base = base;
        this.index = index;
    }

    @Override
    public void add(DTO item) {
        index.add(item, base.addAndLocate(item));
    }

    @Override
    public long size() {
        return base.size();
    }

    @Override
    public Stream<DTO> get() {
        return base.get();
    }

    @Override
    public Stream<DTO> get(Query query) {
        final IndexResult result = index.get(query);
        return result.stream().mapToObj(base::get).filter(result.diff());
    }

    public Index getIndex() {
        return index;
    }

    @Override
    public DTO get(long key) {
        return base.get(key);
    }

    @Override
    public long addAndLocate(DTO q) {
        return base.addAndLocate(q);
    }

    @Override
    public LongStream getIndices(Query query) {
        return index.get(query).stream();
    }

}
