package org.nps.gjoll.index;

import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Query;

public interface Index {

    IndexResult get(Query query);
    
    void add(DTO item, long location);
    
}
