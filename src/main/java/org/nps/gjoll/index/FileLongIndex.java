package org.nps.gjoll.index;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

public class FileLongIndex implements LongIndex<byte[]> {

    final RandomAccessFile file;
    final FileOutputStream out;

    public FileLongIndex(File file) throws IOException {
        this.file = new RandomAccessFile(file, "r");
        this.out = new FileOutputStream(file);
    }

    @Override
    public synchronized byte[] get(long index) {
        try {
            file.seek(index);
            final byte[] bytes = new byte[file.readInt()];
            file.read(bytes);
            return bytes;
        } catch (IOException e) {
            throw new RuntimeException(e); // TODO
        }
    }

    @Override
    public synchronized long addAndLocate(byte[] bytes) {
        try {
            final long length = file.length();
            out.write(ByteBuffer.allocate(4).putInt(bytes.length).array());
            out.write(bytes);
            out.flush();
            return length;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
