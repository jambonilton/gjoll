package org.nps.gjoll;

import java.util.stream.Stream;

public interface DataSink {

    void add(DTO item);

    default void add(Stream<? extends DTO> stream) {
        stream.forEach(this::add);
    }
    
}
