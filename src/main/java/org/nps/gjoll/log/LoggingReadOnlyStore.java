package org.nps.gjoll.log;

import static org.nps.gjoll.net.requests.DataStoreMethod.ADD;
import static org.nps.gjoll.net.requests.DataStoreMethod.AGG;
import static org.nps.gjoll.net.requests.DataStoreMethod.GET;
import static org.nps.gjoll.net.requests.DataStoreMethod.SIZE;

import java.util.stream.Stream;

import org.nps.gjoll.Counts;
import org.nps.gjoll.DTO;
import org.nps.gjoll.ReadOnlyStore;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.query.Query;

public class LoggingReadOnlyStore<D extends ReadOnlyStore> implements ReadOnlyStore {

    final D base;
    final LogWriter log;
    final Format<DTO> format;
    final Format<Query> queryFormat;
    
    public LoggingReadOnlyStore(D base, 
                                LogWriter log,
                                Format<DTO> format,
                                Format<Query> queryFormat) {
        this.base = base;
        this.log = log;
        this.format = format;
        this.queryFormat = queryFormat;
    }

    @Override
    public void add(DTO item) {
        log.info(ADD+" "+format.toString(item));
        base.add(item);
    }

    @Override
    public long size() {
        log.info(SIZE.name());
        return base.size();
    }

    @Override
    public Stream<DTO> get() {
        log.info(GET+" ALL");
        return base.get();
    }

    @Override
    public Stream<DTO> get(Query query) {
        log.info(GET+" "+queryFormat.toString(query));
        return base.get(query);
    }

    @Override
    public Counts counts(Query query, String groupBy) {
        log.info(AGG+" "+queryFormat.toString(query)+" by "+groupBy);
        return base.counts(query, groupBy);
    }
    
}
