package org.nps.gjoll.log;

import java.io.PrintStream;
import java.time.Clock;

public class PrintStreamLogWriter implements LogWriter {

    final String name;
    final PrintStream out;
    final Clock clock;
    
    public PrintStreamLogWriter(String name) {
        this(name, System.out, Clock.systemDefaultZone());
    }
    
    public PrintStreamLogWriter(String name, PrintStream out, Clock clock) {
        this.name = name;
        this.out = out;
        this.clock = clock;
    }

    @Override
    public void info(String msg) {
        out.println(clock.instant()+" INFO "+name+" - "+msg);
    }

    @Override
    public void warn(String msg) {
        out.println(clock.instant()+" WARN "+name+" - "+msg);
    }

    @Override
    public void warn(String msg, Throwable ex) {
        out.println(clock.instant()+" WARN "+name+" - "+msg);
        printException(ex);
    }

    @Override
    public void error(String msg) {
        out.println(clock.instant()+" ERROR "+name+" - "+msg);
    }

    @Override
    public void error(String msg, Throwable ex) {
        out.println(clock.instant()+" ERROR "+name+" - "+msg);
        printException(ex);
    }

    private void printException(Throwable ex) {
        if (ex == null)
            return;
        out.println("    "+ex.getClass().getName()+": "+ex.getMessage());
        for (StackTraceElement elem : ex.getStackTrace())
            out.println("        "+elem.getClassName()+'.'+elem.getMethodName()+":"+elem.getLineNumber());
        // TODO causes
    }
    
}
