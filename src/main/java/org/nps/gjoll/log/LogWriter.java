package org.nps.gjoll.log;

public interface LogWriter {

    void info(String msg);
    void warn(String msg);
    void warn(String msg, Throwable ex);
    void error(String msg);
    void error(String msg, Throwable ex);
    
}
