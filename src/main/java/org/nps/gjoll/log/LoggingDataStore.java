package org.nps.gjoll.log;

import org.nps.gjoll.DTO;
import org.nps.gjoll.DataStore;
import org.nps.gjoll.events.Change;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.updates.Update;

import java.util.function.Consumer;

import static org.nps.gjoll.net.requests.DataStoreMethod.PUT;
import static org.nps.gjoll.net.requests.DataStoreMethod.SUB;

public class LoggingDataStore<D extends DataStore> extends LoggingReadOnlyStore<D> implements DataStore {

    final Format<Update> updateFormat;
    
    public LoggingDataStore(D base, 
                            LogWriter log, 
                            Format<DTO> format,
                            Format<Query> queryFormat,
                            Format<Update> updateFormat) {
        super(base, log, format, queryFormat);
        this.updateFormat = updateFormat;
    }

    @Override
    public void apply(Update update) {
        log.info(PUT+" "+updateFormat.toString(update));
    }

    @Override
    public void onChange(Query criteria, Consumer<Change> action) {
        log.info(SUB+" "+queryFormat.toString(criteria));
    }

}
