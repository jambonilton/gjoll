package org.nps.gjoll.query;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.function.BiFunction;

@SuppressWarnings({"unchecked","rawtypes"})
public enum Comparator implements BiFunction<Object, Object, Boolean>{
    
    equals("=", (a,b)->a.equals(b)), 
    lessThan("<", (a,b)->compare(a,b) < 0), 
    lessThanOrEqual("<=", (a,b)->compare(a,b) <= 0), 
    greaterThan(">", (a,b)->compare(a,b) > 0), 
    greaterThanOrEqual(">=", (a,b)->compare(a,b) >= 0),
    contains("<<", Comparator::contains),
    isIn(">>", (a,b)->contains(b,a));
    
    final String operator;
    final BiFunction<Object,Object,Boolean> compare;
    
    Comparator(String operator,
               BiFunction<Object, Object, Boolean> compare) {
        this.operator = operator;
        this.compare = compare;
    }
    
    public static Comparator forOperand(String op) {
        for (Comparator c : values())
            if (c.operator.equals(op))
                return c;
        throw new UnsupportedOperationException("Unknown operand \""+op+"\".");
    }
    
    public Boolean apply(Object t, Object u) {
		return compare.apply(t, u);
	}

	@Override
    public String toString() {
        return operator;
    }
    
    private static Boolean contains(Object container, Object containee) {
        if (container instanceof Collection)
            return ((Collection) container).contains(containee);
        else if (container instanceof String)
            return ((String) container).contains(String.valueOf(containee));
        throw new IllegalArgumentException(container.getClass().getName()+" not an accepted type for contains operation!");
    }
    
    private static int compare(Object a, Object b) {
        if (a == null)
            return b == null ? 0 : -1;
        else if (b == null)
           return 1;
        if (!(a instanceof Comparable) || !(b instanceof Comparable))
            return a.equals(b) ? 0 : -1;
        if (b.getClass().isAssignableFrom(a.getClass()))
            return ((Comparable) a).compareTo(b);
        if (a instanceof Number && b instanceof Number) {
            return new BigDecimal(a.toString()).compareTo(new BigDecimal(b.toString()));
        }
        throw new IllegalArgumentException("Cannot compare types "+a.getClass()+" and "+b.getClass());
    }
    
}
