package org.nps.gjoll.query;

import org.nps.gjoll.DTO;

public class ExpressionComparison extends Comparison<ValueExpression> {

    public ExpressionComparison(String k, Comparator c, ValueExpression valueExpression) {
        super(k, c, valueExpression);
    }

    @Override
    public boolean test(DTO obj) {
        return comparator.apply(obj, value.getValue(obj));
    }

}
