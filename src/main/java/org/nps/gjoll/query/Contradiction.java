package org.nps.gjoll.query;

import org.nps.gjoll.DTO;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Always false query.
 */
public class Contradiction implements Query {

    @Override
    public boolean test(DTO t) {
        return false;
    }

    @Override
    public Iterator<Query> iterator() {
        return Arrays.<Query>asList(this).iterator();
    }

    @Override
    public Query and(Query query) {
        return this;
    }

    @Override
    public Query or(Query query) {
        return query;
    }

    @Override
    public Query ignoreEmptyParameters() {
        return this;
    }
}
