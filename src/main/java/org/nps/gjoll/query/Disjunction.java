package org.nps.gjoll.query;

import org.nps.gjoll.DTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Disjunction extends ClauseListQuery {

    Disjunction(Query... clauses) {
        super(new ArrayList<>(Arrays.asList(clauses)));
    }
    
    Disjunction(Collection<Query> clauses) {
        super(clauses);
    }

    @Override
    public boolean test(DTO t) {
        for (Predicate<DTO> c : this)
            if (c.test(t))
                return true;
        return false;
    }

    @Override
    public String toString() {
        return super.toString(" | ");
    }

    @Override
    public Query and(Query query) {
        throw new UnsupportedOperationException("Cannot apply AND to disjunction!");
    }

    @Override
    public Query ignoreEmptyParameters() {
        final List<Query> objects = clauses.stream()
                .map(Query::ignoreEmptyParameters)
                .collect(Collectors.toList());
        return new Disjunction(objects);
    }

    @Override
    public Query or(Query query) {
        if (query instanceof Tautology)
            return query;
        else if (query instanceof Contradiction)
            return this;
        this.clauses.add(query);
        return this;
    }
    
}
