package org.nps.gjoll.query;

import java.util.Collection;
import java.util.Iterator;
import java.util.stream.Collectors;

public abstract class ClauseListQuery implements Query {

    protected Collection<Query> clauses;
    
    ClauseListQuery(Collection<Query> clauses) {
        this.clauses = clauses;
    }
    
    @Override
    public boolean hasChildren() {
        return !clauses.isEmpty();
    }

    @Override
    public Iterator<Query> iterator() {
        return clauses.iterator();
    }

    public String toString(CharSequence join) {
        return clauses.stream().map(this::termToString).collect(Collectors.joining(join));
    }
    
    protected String termToString(Query term) {
        return term.hasChildren() ? '('+term.toString()+')' : term.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((clauses == null) ? 0 : clauses.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ClauseListQuery other = (ClauseListQuery) obj;
        if (clauses == null) {
            if (other.clauses != null)
                return false;
        } else if (!clauses.equals(other.clauses))
            return false;
        return true;
    }
    
}
