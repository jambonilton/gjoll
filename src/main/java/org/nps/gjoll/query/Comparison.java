package org.nps.gjoll.query;

import org.nps.gjoll.DTO;
import org.nps.gjoll.io.format.JsonFormat;

import java.util.Arrays;
import java.util.Iterator;

public abstract class Comparison<V> extends Pair<String,V> implements Query {

    private static final long serialVersionUID = 1L;

    final Comparator comparator;
    
    public Comparison(String k, Comparator c, V v) {
        super(k, v);
        this.comparator = c;
    }

    @Override
    public Query and(Query query) {
        return new Conjunction(this, query);
    }

    @Override
    public Query or(Query query) {
        return new Disjunction(this, query);
    }
    
    public Comparator comparator() {
        return comparator;
    }

    @Override
    public Query ignoreEmptyParameters() {
        return new IgnoreEmptyComparison(this);
    }

    @Override
    public boolean test(DTO pairs) {
        return false;
    }

    @Override
    public Iterator<Query> iterator() {
        return Arrays.<Query>asList(this).iterator();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + ((comparator == null) ? 0 : comparator.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Comparison other = (Comparison) obj;
        if (comparator != other.comparator)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return key+comparator+ JsonFormat.asString(value);
    }

}
