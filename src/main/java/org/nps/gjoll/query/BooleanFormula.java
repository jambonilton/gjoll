package org.nps.gjoll.query;

public interface BooleanFormula<T> {

    T and(T other);
    T or(T other);
    
}
