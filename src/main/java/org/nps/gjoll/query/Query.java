package org.nps.gjoll.query;

import org.nps.gjoll.DTO;

import java.util.*;
import java.util.function.Predicate;

/**
 * Holds information for retrieving a set of objects from a datastore.
 */
public interface Query extends Predicate<DTO>, Iterable<Query>, BooleanFormula<Query> {
    
    default boolean hasChildren() { 
        return false;
    }
    
    static Conjunction and(Query... clauses) {
        return and(Arrays.asList(clauses));
    }
    
    static Conjunction and(Collection<Query> clauses) {
        return new Conjunction(clauses);
    }
    
    static Disjunction or(Query... clauses) {
        return or(Arrays.asList(clauses));
    }
    
    static Disjunction or(Collection<Query> clauses) {
        return new Disjunction(clauses);
    }
    
    static Tautology alwaysTrue() {
        return new Tautology();
    }
    
    static Contradiction alwaysFalse() {
        return new Contradiction();
    }

    static Comparison is(String field, Object value) {
        return new BasicComparison(field, Comparator.equals, value);
    }
    
    static Comparison lessThan(String field, Object value) {
        return new BasicComparison(field, Comparator.lessThan, value);
    }
    
    static Comparison greaterThan(String field, Object value) {
        return new BasicComparison(field, Comparator.greaterThan, value);
    }
    
    static Comparison in(String field, Object value) {
        return new BasicComparison(field, Comparator.isIn, value);
    }
    
    static Comparison contains(String field, Object value) {
        return new BasicComparison(field, Comparator.contains, value);
    }
    
    static Comparison compare(String field, Comparator comparator, Object value) {
        return new BasicComparison(field, comparator, value);
    }

    /**
     * Creates a copy of this query that ignores missing fields on comparisons.
     * @return
     */
    Query ignoreEmptyParameters();

}
