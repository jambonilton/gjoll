package org.nps.gjoll.query;

import org.nps.gjoll.DTO;

/**
 * Interface representing an expression that obtains its value from the given
 * variable context (i.e. the supplied queryable object).  For example, the right
 * side of the given assignment "a = a + 1" could be used to increment "a".
 */
public interface ValueExpression {
    Object getValue(DTO context);
}
