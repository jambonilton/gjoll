package org.nps.gjoll.query;

import org.nps.gjoll.DTO;

public class IgnoreEmptyComparison extends BasicComparison {
    public <V> IgnoreEmptyComparison(Comparison<V> c) {
        super(c.getKey(), c.comparator(), c.getValue());
    }

    @Override
    public boolean test(DTO t) {
        if (t.get(key) == null)
            return true;
        return super.test(t);
    }
}
