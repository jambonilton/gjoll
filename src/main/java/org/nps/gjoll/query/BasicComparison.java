package org.nps.gjoll.query;

import org.nps.gjoll.DTO;

public class BasicComparison extends Comparison<Object> {

    public BasicComparison(String k, Comparator c, Object o) {
        super(k, c, o);
    }

    @Override
    public boolean test(DTO t) {
        return comparator.compare.apply(t.get(key), value);
    }
}
