package org.nps.gjoll.query;

import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.Input;
import org.nps.gjoll.io.PeekingInputStream;
import org.nps.gjoll.io.SerializationException;
import org.nps.gjoll.io.format.BadCharacterException;
import org.nps.gjoll.io.format.JsonFormat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Character.isAlphabetic;
import static java.lang.Character.isLetterOrDigit;

public class StandardQueryFormat implements Format<Query> {
    
    @Override
    public void write(Query t, OutputStream out) {
        try {
            out.write(t.toString().getBytes());
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }

    @Override
    public Query read(InputStream in) throws IOException {
        return readQuery(new PeekingInputStream(in), -1);
    }

    @Override
    public Query read(Input in) throws IOException {
        return in instanceof PeekingInputStream ? readQuery((PeekingInputStream) in, -1) : read(in.getInputStream());
    }

    private Query readQuery(PeekingInputStream in, int terminator) throws IOException {
        final List<Query> terms = new ArrayList<>();
        int joiner = -1;
        int ch = in.skipWhiteSpace().peek();
        while (ch != terminator) {
            // read term
            if (isAlphabetic(ch))
                terms.add(readComparison(in));
            else if (ch == '(')
                terms.add(readSubQuery(in));
            else
                throw new BadCharacterException(ch, in.getIndex(), "abcdefghijklmnopqrstuvwxyz(");
            
            // read joiner
            ch = in.skipWhiteSpace().read();
            if (ch == terminator)
                continue;
            else if (ch == '&' || ch == '|') {
                if (joiner == -1)
                    joiner = ch;
                else if (ch != joiner)
                    throw new NormalFormRequiredException(ch, in.getIndex(), (char) joiner);
            }
            else
                throw new BadCharacterException(ch, in.getIndex(), (char) joiner);
            
            ch = in.skipWhiteSpace().peek();
        }
        switch (joiner) {
        case '|':
            return new Disjunction(terms);
        case '&':
        default:
            return new Conjunction(terms);
        }
    }

    private Query readSubQuery(PeekingInputStream in) throws IOException {
        in.read(); // burn (
        return readQuery(in, ')');
    }

    private Comparison readComparison(PeekingInputStream in) throws IOException {
        return new BasicComparison(readField(in), readOperator(in), readValue(in));
    }

    private String readField(PeekingInputStream in) throws IOException {
        final StringBuilder sb = new StringBuilder();
        int ch = in.peek();
        while (isLetterOrDigit(ch)) {
            sb.append((char) in.read());
            ch = in.peek();
        }
        return sb.toString();
    }
    
    private Comparator readOperator(PeekingInputStream in) throws IOException {
        final StringBuilder sb = new StringBuilder();
        int ch = in.skipWhiteSpace().peek();
        if (!isComparator(ch))
            throw new BadCharacterException(ch, in.getIndex(), "=<>");
        while (isComparator(ch)) {
            sb.append((char) in.read());
            ch = in.peek();
        }
        return Comparator.forOperand(sb.toString());
    }
    
    private boolean isComparator(int ch) {
        switch (ch) {
        case '=': case '<': case '>': return true;
        default: return false;
        }
    }

    private Object readValue(PeekingInputStream in) throws IOException {
        return JsonFormat.readValue(in.skipWhiteSpace());
    }
    
    
}
