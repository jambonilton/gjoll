package org.nps.gjoll.query;

public enum SortOrder {
    ASC, DESC
}
