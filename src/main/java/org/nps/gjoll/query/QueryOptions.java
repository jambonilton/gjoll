package org.nps.gjoll.query;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface QueryOptions {

    Optional<Set<String>> fields();

    Collection<Pair<String, SortOrder>> sorting();

}
