package org.nps.gjoll.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public class SelectQuery implements QueryOptions {

    final Optional<Set<String>> fields;

    Collection<Pair<String, SortOrder>> sorting;
    Query query;

    public SelectQuery(Optional<Set<String>> fields) {
        this.fields = fields;
        this.sorting = new ArrayList<>();
    }

    public Collection<Pair<String, SortOrder>> sorting() {
        return sorting;
    }

    public Optional<Set<String>> fields() {
        return fields;
    }

    public Query query() {
        return query;
    }
}
