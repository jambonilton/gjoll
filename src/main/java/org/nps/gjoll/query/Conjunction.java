package org.nps.gjoll.query;

import org.nps.gjoll.DTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Conjunction extends ClauseListQuery {
    
    Conjunction(Query... clauses) {
        super(new ArrayList<>(Arrays.asList(clauses)));
    }
    
    Conjunction(Collection<Query> clauses) {
        super(clauses);
    }

    @Override
    public boolean test(DTO t) {
        for (Predicate<DTO> c : this)
            if (!c.test(t))
                return false;
        return true;
    }

    @Override
    public String toString() {
        return super.toString(" & ");
    }

    @Override
    public Query and(Query query) {
        if (query instanceof Tautology)
            return this;
        else if (query instanceof Contradiction)
            return query;
        clauses.add(query);
        return this;
    }

    @Override
    public Query ignoreEmptyParameters() {
        final List<Query> objects = clauses.stream()
                .map(Query::ignoreEmptyParameters)
                .collect(Collectors.toList());
        return new Conjunction(objects);
    }

    @Override
    public Query or(Query query) {
        throw new UnsupportedOperationException("Cannot apply OR to conjunction!");
    }
    
}
