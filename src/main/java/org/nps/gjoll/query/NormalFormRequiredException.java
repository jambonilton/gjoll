package org.nps.gjoll.query;

import org.nps.gjoll.io.format.BadCharacterException;

public class NormalFormRequiredException extends BadCharacterException {

    private static final long serialVersionUID = 1L;

    public NormalFormRequiredException(int actual, int index, char... expected) {
        super(actual, index, expected);
    }
    
}
