package org.nps.gjoll.streams;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ConcatenatedIterator<T> implements Iterator<T> {

    final Iterator<Iterator<? extends T>> iterators;
    
    Iterator<? extends T> current;

    @SafeVarargs
    public ConcatenatedIterator(Iterator<? extends T>... iterators) {
        final List<Iterator<? extends T>> iters = new ArrayList<>();
        for (Iterator<? extends T> iter : iterators)
            if (iter.hasNext())
                iters.add(iter);
        this.iterators = iters.iterator();
    }

    @Override
    public boolean hasNext() {
        return current.hasNext() || iterators.hasNext();
    }

    @Override
    public T next() {
        if (!current.hasNext())
            current = iterators.next();
        return current.next();
    }
    
    
    
}
