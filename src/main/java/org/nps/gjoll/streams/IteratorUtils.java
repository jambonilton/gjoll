package org.nps.gjoll.streams;

import java.util.Iterator;
import java.util.function.Function;
import java.util.function.Predicate;

public class IteratorUtils {

    public static <E> Iterator<E> concat(Iterator<? extends E>... iterators) {
        return new ConcatenatedIterator<>(iterators);
    }

    public static <E> Iterator<E> filter(Iterator<E> iterator, Predicate<E> predicate) {
        return new FilteredIterator<>(iterator, predicate);
    }

    public static <I,O> Iterator<O> transform(Iterator<I> iterator, Function<I,O> transform) {
        return new TransformingIterator<>(iterator, transform);
    }

    public static <I,O> Iterable<O> transform(Iterable<I> iterable, Function<I,O> transform) {
        return () -> new TransformingIterator<>(iterable.iterator(), transform);
    }

}
