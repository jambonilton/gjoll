package org.nps.gjoll.streams;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class StreamUtils {

    /**
     * Merge two sorted streams.
     * TODO apply deduping too
     */
    public static <T extends Comparable<T>> Stream<T> merge(Stream<T> stream1, Stream<T> stream2) {
        Iterator<T> iterator = new MergedIterator<>(stream1.iterator(), stream2.iterator());
        Spliterator<T> spliterator = Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED);
        return StreamSupport.stream(spliterator, false);
    }
    
}
