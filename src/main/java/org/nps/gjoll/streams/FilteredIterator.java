package org.nps.gjoll.streams;

import java.util.Iterator;
import java.util.function.Predicate;

public class FilteredIterator<E> implements Iterator<E> {

    final Iterator<E> base;
    final Predicate<E> predicate;

    E next;

    public FilteredIterator(Iterator<E> base, Predicate<E> predicate) {
        this.base = base;
        this.predicate = predicate;
        next = advance();
    }

    @Override
    public boolean hasNext() {
        return next != null;
    }

    @Override
    public E next() {
        E current = next;
        next = advance();
        return current;
    }

    private E advance() {
        while (base.hasNext()) {
            final E e = base.next();
            if (predicate.test(e))
                return e;
        }
        return null;
    }

}
