package org.nps.gjoll.streams;

import java.util.Iterator;
import java.util.function.Function;

public class TransformingIterator<I,O> implements Iterator<O> {

    final Iterator<I> base;
    final Function<I,O> transform;

    public TransformingIterator(Iterator<I> base, Function<I, O> transform) {
        this.base = base;
        this.transform = transform;
    }

    @Override
    public boolean hasNext() {
        return base.hasNext();
    }

    @Override
    public O next() {
        return transform.apply(base.next());
    }
}
