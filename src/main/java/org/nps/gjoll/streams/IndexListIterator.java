package org.nps.gjoll.streams;

import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.nps.gjoll.query.Pair;

public class IndexListIterator<T> implements Iterator<Pair<Integer, T>> {

    final List<? extends T> list;
    int index;

    public static <T> Stream<Pair<Integer, T>> stream(List<? extends T> list) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new IndexListIterator<T>(list), Spliterator.DISTINCT), false);
    }
    
    public static <T> IndexListIterator<T> from(List<? extends T> list) {
        return new IndexListIterator<T>(list);
    }
    
    public IndexListIterator(List<? extends T> list) {
        this.list = list;
        this.index = 0;
    }

    @Override
    public boolean hasNext() {
        return index < list.size();
    }

    @Override
    public Pair<Integer, T> next() {
        return new Pair<>(index, list.get(index++));
    }

}
