package org.nps.gjoll;

import org.nps.gjoll.events.Change;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.updates.Update;

import java.util.function.Consumer;
import java.util.stream.Stream;

public interface Store<E> {

    void add(E item);

    void add(Stream<E> stream);

    long size();

    Stream<E> get();

    Stream<E> get(String str);

    Stream<E> get(Query query);

    Counts counts(String query, String groupBy);

    Counts counts(Query query, String groupBy);

    void apply(Update update);

    void onChange(Query criteria, Consumer<Change> action);

}