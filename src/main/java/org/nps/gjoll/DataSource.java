package org.nps.gjoll;

import org.nps.gjoll.query.Pair;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.std.MapCounts;
import org.nps.gjoll.std.MappedDataSource;
import org.nps.gjoll.updates.OverrideDTO;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.nps.gjoll.query.Query.is;

public interface DataSource {

    long size();
    
    Stream<DTO> get();
    
    default Stream<DTO> get(String str) {
        return get(Gjoll.getQueryFormat().read(str));
    }
    
    default Stream<DTO> get(Query query) {
        return get().filter(query);
    }

    /**
     * Performs a one-to-many join on the given data source on leftField == rightField.
     * @param right
     * @param leftField
     * @param rightField
     * @return
     */
    default DataSource join(DataSource right, String leftField, String rightField) {
        return join(right, (q)->is(rightField, q.get(leftField)), (q, s)->new OverrideDTO(q, asList(new Pair<>(leftField, s.collect(toList())))));
    }

    /**
     * Generic join method.
     * @param right
     * @param queryFunction
     * @param mergeFunction
     * @return
     */
    default DataSource join(DataSource right, Function<DTO, Query> queryFunction, BiFunction<DTO, Stream<DTO>, DTO> mergeFunction) {
        return map((q)->mergeFunction.apply(q, right.get(queryFunction.apply(q))));
    }

    default DataSource map(UnaryOperator<DTO> map) {
        return new MappedDataSource(this, map);
    }

    default Counts counts(String query, String groupBy) {
        return counts(Gjoll.getQueryFormat().read(query), groupBy);
    }
    
    default Counts counts(Query query, String groupBy) {
        final MapCounts counts = new MapCounts();
        get(query).forEach((e)->counts.increment(e.get(groupBy)));
        return counts;
    }
    
}
