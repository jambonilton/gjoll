package org.nps.gjoll;

import org.nps.gjoll.query.Pair;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;

import static org.nps.gjoll.reflect.QueryableReflector.reflect;

public interface DTO extends Iterable<Pair<String, Object>>, Serializable {

    /**
     * Get the number of fields present on this object.
     */
    default int size() {
        return reflect(this).size();
    }

    /**
     * Get the value of a given field.  Returns null if not present.
     */
    default Object get(String field) {
        return reflect(this).get(this, field);
    }

    /**
     * Get the collection of fields present on this object.
     */
    default Collection<String> fields() {
        return reflect(this).fields();
    }

    /**
     * Get all values of this object in order of fields.
     */
    default Collection<Object> values() {
        return reflect(this).values(this);
    }

    /**
     * Convert to given object via reflective operations.
     */
    default <T> T asA(Class<T> type) {
        try {
            final T instance = type.newInstance();
            for (final Field field : type.getDeclaredFields()) {
                if (!Modifier.isStatic(field.getModifiers())) {
                    field.setAccessible(true);
                    field.set(instance, get(field.getName()));
                }
            }
            return instance;
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

}
