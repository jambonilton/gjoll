package org.nps.gjoll.io;

import java.io.IOException;
import java.io.InputStream;

public class InputStreamWrapper implements Input {

    final InputStream in;
    
    public InputStreamWrapper(InputStream in) {
        this.in = in;
    }

    @Override
    public int read() throws IOException {
        return in.read();
    }

    @Override
    public void close() throws IOException {
        in.close();
    }

    @Override
    public InputStream getInputStream() {
        return in;
    }

    @Override
    public String readString(int count) throws IOException {
        final byte[] b = new byte[count];
        in.read(b);
        return new String(b);
    }
}
