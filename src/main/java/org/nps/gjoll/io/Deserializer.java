package org.nps.gjoll.io;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface Deserializer<T> {
    
    T read(InputStream in) throws IOException;

    default T read(Input in) throws IOException {
        return read(in.getInputStream());
    }

    default T read(String in) {
        try {
            return read(new ByteArrayInputStream(in.getBytes()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    default Stream<T> stream(InputStream in) {
        final DeserializerIterator<T> iterator = new DeserializerIterator<>(new PeekingInputStream(in), this);
        final Spliterator<T> spliterator = Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED);
        return StreamSupport.stream(spliterator, false);
    }
    
    class DeserializationException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        public DeserializationException(Throwable cause) {
            super(cause);
        }
        
    }
    
}
