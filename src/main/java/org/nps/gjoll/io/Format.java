package org.nps.gjoll.io;

public interface Format<T> extends Serializer<T>, Deserializer<T> {

}
