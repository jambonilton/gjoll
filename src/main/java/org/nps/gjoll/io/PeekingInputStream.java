package org.nps.gjoll.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.function.IntPredicate;

public class PeekingInputStream implements AutoCloseable, Input {
    final InputStream inputStream;
    final Reader base;

    Integer peek;
    int index = 0;

    public PeekingInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
        this.base = new InputStreamReader(inputStream);
    }

    public PeekingInputStream skipWhiteSpace() throws IOException {
        int ch = -1;
        while (Character.isWhitespace(ch = read()))
            ;
        setPeek(ch);
        return this;
    }

    public String readTo(IntPredicate condition) throws IOException {
        final StringBuilder sb = new StringBuilder();
        int next = read();
        while (next != -1 && !condition.test(next)) {
            sb.append((char) next);
            next = read();
        }
        return sb.toString();
    }

    @Override
    public int read() throws IOException {
        if (peek != null) {
            int ret = peek;
            peek = null;
            return ret;
        }
        index++;
        return base.read();
    }

    @Override
    public String readString(int count) throws IOException {
        final StringBuilder sb = new StringBuilder();
        for (int i=0; i < count; i++)
            sb.append((char) read());
        return sb.toString();
    }

    // override the peek value for reversing
    public void setPeek(final Integer peek) {
        this.peek = peek;
    }

    public int peek() throws IOException {
        return peek = read();
    }

    @Override
    public void close() throws IOException {
        base.close();
    }

    public int getIndex() {
        return index;
    }

    @Override
    public InputStream getInputStream() {
        return inputStream;
    }

}
