package org.nps.gjoll.io;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * Helper class for getting binary data from input streams.
 *
 * Note, not thread-safe.
 */
public class InputBuffer implements AutoCloseable {

    final byte[] longBuffer = new byte[8],
                 intBuffer = new byte[4],
                 shortBuffer = new byte[2];

    final InputStream in;

    public InputBuffer(InputStream in) {
        this.in = in;
    }

    public short readShort() throws IOException {
        return read(shortBuffer).getShort();
    }

    public int readInt() throws IOException {
        return read(intBuffer).getInt();
    }

    public long readLong() throws IOException {
        return read(longBuffer).getLong();
    }

    public float readFloat() throws IOException {
        return read(intBuffer).getInt();
    }

    public double readDouble() throws IOException {
        return read(longBuffer).getDouble();
    }

    public String readString(int length) throws IOException {
        return new String(read(length));
    }

    public byte[] read(int length) throws IOException {
        byte[] buffer = new byte[length];
        in.read(buffer);
        return buffer;
    }

    public ByteBuffer read(byte[] buffer) throws IOException {
        in.read(buffer);
        return ByteBuffer.wrap(buffer);
    }

    public int read() throws IOException {
        return in.read();
    }

    @Override
    public void close() throws IOException {
        in.close();
    }
}
