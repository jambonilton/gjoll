package org.nps.gjoll.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

public class RandomAccessFileInput implements Input {

    final RandomAccessFile file;

    public RandomAccessFileInput(RandomAccessFile file) {
        this.file = file;
    }

    @Override
    public int read() throws IOException {
        return file.read();
    }

    @Override
    public void close() throws IOException {
        file.close();
    }

    @Override
    public InputStream getInputStream() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String readString(int count) throws IOException {
        final byte[] b = new byte[count];
        file.read(b);
        return new String(b);
    }
}
