package org.nps.gjoll.io;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.stream.Stream;

public interface Serializer<T> {
    
    void write(T t, OutputStream out);
    
    default String toString(T t) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        write(t, out);
        return out.toString();
    }
    
    default void write(Stream<? extends T> stream, OutputStream out) {
        stream.forEach((t)->write(t, out));
    }
    
}