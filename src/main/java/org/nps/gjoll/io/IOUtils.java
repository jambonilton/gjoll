package org.nps.gjoll.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.function.Function;

public class IOUtils {

    public final static Function<byte[], ByteBuffer> wrap = ByteBuffer::wrap;
    
    public static String read(final InputStream in) throws IOException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[256];
        int n;
        while ((n = in.read(buffer)) != -1)
            out.write(buffer, 0, n);
        return new String(out.toByteArray());
    }

    public static String readLine(InputStream in) throws IOException {
        final StringBuilder sb = new StringBuilder();
        int c;
        while (!isEndOfLine(c = in.read()))
            sb.append((char) c);
        return sb.toString();
    }

    private static boolean isEndOfLine(int i) {
        return i=='\n' || i==-1;
    }

}
