package org.nps.gjoll.io;

import org.nps.gjoll.io.Deserializer.DeserializationException;

import java.util.Iterator;

/**
 * Repeatedly calls the deserializer on an input stream.  This assumes that the deserializer will return null when EOF is returned.
 */
public class DeserializerIterator<T> implements Iterator<T> {

    final Input in;
    final Deserializer<T> deserializer;
    
    T next = null;
    
    public DeserializerIterator(Input in, Deserializer<T> deserializer) {
        this.in = in;
        this.deserializer = deserializer;
        advance();
    }

    @Override
    public boolean hasNext() {
        return next != null;
    }

    @Override
    public T next() {
        final T current = next;
        advance();
        return current;
    }

    private void advance() {
        try {
            next = deserializer.read(in);
        } catch (Exception e) {
            throw new DeserializationException(e);
        }
    }
    
}