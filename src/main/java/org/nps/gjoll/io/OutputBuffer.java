package org.nps.gjoll.io;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * Buffered output stream with support for different primitive types.
 */
public class OutputBuffer {

    final OutputStream out;
    final ByteBuffer buffer;

    public OutputBuffer(OutputStream out) {
        this(out, 512);
    }

    public OutputBuffer(OutputStream out, int bufferSize) {
        this(out, ByteBuffer.allocate(bufferSize));
    }

    public OutputBuffer(OutputStream out, ByteBuffer buffer) {
        this.out = out;
        this.buffer = buffer;
    }

    public OutputBuffer writeShort(short value) throws IOException {
        makeRoomFor(2);
        buffer.putShort(value);
        return this;
    }

    public OutputBuffer writeInt(int value) throws IOException {
        makeRoomFor(4);
        buffer.putInt(value);
        return this;
    }

    public OutputBuffer writeLong(long value) throws IOException {
        makeRoomFor(8);
        buffer.putLong(value);
        return this;
    }

    public OutputBuffer writeFloat(float value) throws IOException {
        makeRoomFor(4);
        buffer.putFloat(value);
        return this;
    }

    public OutputBuffer writeDouble(double value) throws IOException {
        makeRoomFor(8);
        buffer.putDouble(value);
        return this;
    }

    public OutputBuffer write(byte value) throws IOException {
        makeRoomFor(1);
        buffer.put(value);
        return this;
    }

    public OutputBuffer write(byte[] bytes) throws IOException {
        if (bytes.length > buffer.capacity()) {
            flush();
            out.write(bytes);
        } else {
            makeRoomFor(bytes.length);
            buffer.put(bytes);
        }
        return this;
    }

    private void makeRoomFor(int bytes) throws IOException {
        if (buffer.remaining() > bytes)
            return;
        flush();
    }

    public void flush() throws IOException {
        out.write(buffer.array(), 0, buffer.position());
        buffer.rewind();
    }

}
