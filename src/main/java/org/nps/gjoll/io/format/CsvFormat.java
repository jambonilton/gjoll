package org.nps.gjoll.io.format;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.nps.gjoll.DTO;
import org.nps.gjoll.io.QueryableFormat;
import org.nps.gjoll.query.Pair;
import org.nps.gjoll.std.ArrayDTO;
import org.nps.gjoll.tables.TableDescriptor;

/**
 * Serializes objects into a CSV format, or can use custom delimiters for a custom format.
 */
public class CsvFormat implements QueryableFormat {

    final String columnDelimiter;
    final String rowDelimiter;
    final Collector<CharSequence,?,String> collector;
    final TableDescriptor tableDescriptor;
    
    /**
     * Default constructor, uses CSV.
     */
    public CsvFormat(TableDescriptor tableDescriptor) {
        this(",", "\n", tableDescriptor);
    }
    
    public CsvFormat(String columnDelimiter, String rowDelimiter, TableDescriptor tableDescriptor) {
        this.columnDelimiter = columnDelimiter;
        this.rowDelimiter = rowDelimiter;
        collector = Collectors.joining(columnDelimiter);
        this.tableDescriptor = tableDescriptor;
    }

    @Override
    public void write(DTO q, OutputStream out) {
        try {
            out.write(tableDescriptor.values(q).map(Pair::getValue).map(String::valueOf).collect(collector).getBytes());
            out.write('\n');
            out.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public DTO read(InputStream in) throws IOException {
        final StringBuilder sb = new StringBuilder();
        int ch = in.read();
        while (ch != '\n' && ch != -1)
            sb.append((char) ch);
        final String[] strings = sb.toString().split(columnDelimiter);
        if (strings.length != tableDescriptor.size())
            throw new ColumnNumberMismatchException(tableDescriptor.size(), strings.length);
        final Object[] values = new Object[strings.length];
        for (int i=0; i < strings.length; i++) {
            try {
                values[i] = tableDescriptor.get(i).read(strings[i]);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return new ArrayDTO(tableDescriptor.indices(), values);
    }
    
    public static class ColumnNumberMismatchException extends RuntimeException {

        private static final long serialVersionUID = 1L;
        
        public ColumnNumberMismatchException(int expected, int actual) {
            super(String.format("Expected %d columns, but found %d.", expected, actual));
        }
        
    }
    
}
