package org.nps.gjoll.io.format;

import org.nps.gjoll.io.SerializationException;

public class BadCharacterException extends SerializationException {

    private static final long serialVersionUID = 1L;
    
    public BadCharacterException(int actual, int index, char... expected) {
        this(actual, index, new String(expected));
    }
    
    public BadCharacterException(int actual, int index, String expected) {
        super(String.format("Expected one of %s but was %s at index %d.", new String(expected), actual == -1 ? "EOF" : (char) actual, index));
    }
    
}