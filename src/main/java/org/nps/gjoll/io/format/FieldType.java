package org.nps.gjoll.io.format;

public enum FieldType {
    CHAR,
    STRING,
    BOOLEAN,
    BYTE,
    SHORT,
    INTEGER,
    LONG,
    FLOAT,
    DOUBLE,
    OBJECT
}
