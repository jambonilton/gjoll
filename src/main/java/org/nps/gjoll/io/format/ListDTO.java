package org.nps.gjoll.io.format;

import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class ListDTO implements DTO {

    private static final long serialVersionUID = 1L;

    final List<Pair<String, Object>> values;
    
    public ListDTO() {
        this(new ArrayList<>());
    }
    
    public ListDTO(Iterable<Pair<String, Object>> values) {
        this.values = new ArrayList<>();
        for (Pair<String, Object> pair : values)
            this.values.add(pair);
    }
    
    public ListDTO(List<Pair<String, Object>> values) {
        this.values = values;
    }

    @Override
    public Iterator<Pair<String, Object>> iterator() {
        return values.iterator();
    }

    @Override
    public Object get(String field) {
        for (Pair<String, Object> t : values)
            if (t.getKey().equals(field))
                return t.getValue();
        return null;
    }

    public ListDTO putAll(Iterable<Pair<String,Object>> values) {
        for (Pair<String, Object> value : values)
            this.values.add(value);
        return this;
    }
    
    public ListDTO put(String key, Object value) {
        values.add(new Pair<>(key, value));
        return this;
    }

    @Override
    public Collection<String> fields() {
        return values.stream().map((t)->t.getKey()).collect(Collectors.toList());
    }

    @Override
    public Collection<Object> values() {
        return values.stream().map((t)->t.getValue()).collect(Collectors.toList());
    }

    @Override
    public int size() {
        return values.size();
    }
}
