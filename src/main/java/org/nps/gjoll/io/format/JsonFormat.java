package org.nps.gjoll.io.format;

import org.nps.gjoll.DTO;
import org.nps.gjoll.io.Input;
import org.nps.gjoll.io.PeekingInputStream;
import org.nps.gjoll.io.QueryableFormat;
import org.nps.gjoll.io.SerializationException;
import org.nps.gjoll.query.Pair;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class JsonFormat implements QueryableFormat {

    @Override
    public void write(DTO object, OutputStream out) {
        try {
            writeObject(object, out);
        } catch (IOException e) {
            throw new JsonFormatException(e);
        }
    }

    public static void writeObject(Iterable<Pair<String, Object>> object, OutputStream out) throws IOException {
        writeObject(object.iterator(), out);
    }

    public static void writeObject(Iterator<Pair<String, Object>> tuples, OutputStream out) throws IOException {
        out.write('{');
        if (tuples.hasNext()) {
            writeTuple(out, tuples.next());
            while (tuples.hasNext()) {
                out.write(',');
                writeTuple(out, tuples.next());
            }
        }
        out.write('}');
    }

    public static void writeTuple(OutputStream out, Pair<String, Object> tuple) throws IOException {
        out.write('"');
        out.write(tuple.getKey().getBytes());
        out.write('"');
        out.write(':');
        final Object value = tuple.getValue();
        writeValue(out, value);
    }

    // TODO not nice.
    public static String asString(Object value) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            writeValue(out, value);
        } catch (IOException e) {
            throw new SerializationException(e);
        }
        return out.toString();
    }
    
    @SuppressWarnings("rawtypes")
    public static void writeValue(OutputStream out, final Object value)
            throws IOException {
        if (value instanceof DTO)
            writeObject((DTO) value, out);
        else if (value instanceof String)
            out.write(("\""+value+"\"").getBytes());
        else if (value instanceof Iterable) {
            out.write('[');
            Iterator iter = ((Iterable) value).iterator();
            if (iter.hasNext()) {
                writeValue(out, iter.next());
                while (iter.hasNext()) {
                    out.write(',');
                    writeValue(out, iter.next());
                }
            }
            out.write(']');
        }
        else
            out.write(String.valueOf(value).getBytes());
    }

    @Override
    public DTO read(InputStream inputStream) throws IOException {
        return readFirstObject(new PeekingInputStream(inputStream));
    }

    @Override
    public DTO read(Input in) throws IOException {
        return in instanceof PeekingInputStream ? readFirstObject((PeekingInputStream) in) : read(in.getInputStream());
    }

    public DTO readFirstObject(PeekingInputStream in) throws IOException {
        if (in.skipWhiteSpace().peek() == -1)
            return null;
        return readObject(in);
    }

    public static DTO readObject(PeekingInputStream in) throws IOException {
        int ch = in.skipWhiteSpace().read();
        if (ch != '{')
            throw new BadCharacterException(ch, in.getIndex(), '{');
        final List<Pair<String, Object>> assignments = new ArrayList<Pair<String,Object>>();
        while (ch != '}') {
            final String field = readStringLiteral(in);
            ch = in.skipWhiteSpace().read();
            if (ch != ':')
                throw new BadCharacterException(ch, in.getIndex(), ':');
            final Object value = readValue(in.skipWhiteSpace());
            assignments.add(new Pair<>(field.toString(), value));
            ch = in.skipWhiteSpace().read();
        }
        return new ListDTO(assignments);
    }

    public static String readStringLiteral(PeekingInputStream in) throws IOException {
        final StringBuilder field = new StringBuilder();
        final int quote = in.skipWhiteSpace().read();
        if (quote != '"' && quote != '\'')
            throw new BadCharacterException(quote, in.getIndex(), '"', '\'');
        int ch = in.read();
        while (ch != quote) {
            if (ch == -1)
                throw new PrematureFinishException(in.getIndex(), '}');
            if (ch == '\\')
                ch = getEscaped(in);
            field.append((char) ch);
            ch = in.read();
        }
        return field.toString();
    }

    public static char getEscaped(final Input in) throws IOException
    {
        char ch = (char) in.read();
        switch (ch) {
        case 'b':
            return '\b';
        case 'f':
            return '\f';
        case 'n':
            return '\n';
        case 'r':
            return '\r';
        case 't':
            return '\t';
        case 'u':
            // TODO unicode char support
            // TODO quote support
        default:
            return ch;
        }
    }

    public static Object readValue(PeekingInputStream in) throws IOException {
        char peek = (char) in.peek();
        switch (peek) {
        case '{':
            return readObject(in);
        case '[':
            return readArray(in);
        case '\'':
        case '"':
            return readStringLiteral(in);
        case '-':
        case '0': case '1': case '2': case '3': case '4': 
        case '5': case '6': case '7': case '8': case '9':
            return readNumber(in);
        case 't':
            return readTrueBooleanLiteral(in);
        case 'f':
            return readFalseBooleanLiteral(in);
        case 'n':
            return readNullLiteral(in);
        }
        throw new BadCharacterException(peek, in.getIndex(), "{['\"0-9tfn");
    }


    public static Collection<Object> readArray(PeekingInputStream in) throws IOException
    {
        in.setPeek(null); // burn the ]
        int read;
        final Collection<Object> children = new ArrayList<>();
        while ((read = in.skipWhiteSpace().peek()) != ']') {
            if (read == -1)
                throw new PrematureFinishException(in.getIndex(), ']');
            children.add(readValue(in));
            read = in.read();
            if (read == ',')
                continue;
            else if (read == ']')
                break;
            else
                throw new BadCharacterException(read, in.getIndex(), ',', ']');
        }
        in.setPeek(null);
        return children;
    }
    
    public static Number readNumber(PeekingInputStream in) throws IOException
    {
        final StringBuilder sb = new StringBuilder();
        boolean hasDecimal=false, hasExponent=false;
        while (true) {
            int read = in.read();
            if (read == '.')
                hasDecimal = true;
            else if (read == 'e' || read == 'E')
                hasExponent = true;
            else if (!isDigit(read) && read != '-' && read != '+')
            {
                in.setPeek(read);
                break;
            }
            sb.append((char) read);
        }
        if (hasDecimal || hasExponent)
            return Double.parseDouble(sb.toString());
        if (sb.length() > 9)
            return Long.parseLong(sb.toString());
        return Integer.parseInt(sb.toString());
    }
    
    public static boolean isDigit(int read)
    {
        return read >= '0' && read <= '9';
    }
    
    public static Boolean readTrueBooleanLiteral(Input in) throws IOException
    {
        return new Boolean(in.readString(4));
    }
    
    public static Boolean readFalseBooleanLiteral(Input in) throws IOException
    {
        return new Boolean(in.readString(5));
    }
    
    public static Object readNullLiteral(Input in) throws IOException
    {
        final String s = in.readString(4);
        if (!s.equals("null"))
            throw new JsonParseException("Expected null, but was \""+s+"\".");
        return null;
    }
    
    public static class PrematureFinishException extends SerializationException {

        private static final long serialVersionUID = 1L;

        public PrematureFinishException(int index, char expected) {
            super(String.format("Unexpected end of input at index %d. Expected %c.", index, (char) expected));
        }
        
    }
    
    public static class JsonParseException extends SerializationException {

        private static final long serialVersionUID = 1L;

        public JsonParseException(String s) {
            super(s);
        }
        
    }
    
    public static class JsonFormatException extends SerializationException {

        private static final long serialVersionUID = 1L;

        public JsonFormatException(Throwable cause) {
            super(cause);
        }
        
    }
    
}
