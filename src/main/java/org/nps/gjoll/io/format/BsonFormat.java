package org.nps.gjoll.io.format;

import org.nps.gjoll.DTO;
import org.nps.gjoll.index.LongBiIndex;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.InputBuffer;
import org.nps.gjoll.io.OutputBuffer;
import org.nps.gjoll.io.SerializationException;
import org.nps.gjoll.query.Pair;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * TODO I feel sorry to anyone who has to look at this code...
 */
public class BsonFormat implements Format<DTO> {

    private static final int ASSIGNMENT_SIZE = 2+1;

    private interface ReadObjectValue {
        Object read(LongBiIndex<String> fields, InputBuffer in) throws IOException;
    }

    private interface ReadValue extends ReadObjectValue {
        default Object read(LongBiIndex<String> fields, InputBuffer in) throws IOException {
            return read(in);
        }
        Object read(InputBuffer in) throws IOException;
    }

    private interface WriteObjectValue {
        void write(LongBiIndex<String> fields, OutputBuffer out, Object value) throws IOException;
    }

    private interface WriteValue extends WriteObjectValue {
        default void write(LongBiIndex<String> fields, OutputBuffer out, Object value) throws IOException {
            write(out, value);
        }
        void write(OutputBuffer out, Object value) throws IOException;
    }

    enum ValueType {

        OBJECT1((f,in)->readObject(f,in,in.read()), (f,o,v)->writeObject(f,o,(DTO)v)),
        OBJECT2((f,in)->readObject(f,in,in.readShort()), (f,o,v)->writeObject(f,o,(DTO)v)),
        OBJECT4((f,in)->readObject(f,in,in.readInt()), (f,o,v)->writeObject(f,o,(DTO)v)),
        ARRAY1((f,in)->readArray(f,in,in.read()), (f,o,v)->writeCollection(f,o.write((byte)((Collection)v).size()),(Collection) v)),
        ARRAY2((f,in)->readArray(f,in,in.readShort()), (f,o,v)->writeCollection(f,o.writeShort((short)((Collection)v).size()),(Collection) v)),
        ARRAY4((f,in)->readArray(f,in,in.readInt()), (f,o,v)->writeCollection(f,o.writeInt(((Collection)v).size()),(Collection) v)),
        STRING1((in)->new String(in.read(in.read())), (o,v)->writeString(o,(String)v,1)),
        STRING2((in)->new String(in.read(in.readShort())), (o,v)->writeString(o,(String)v,2)),
        STRING4((in)->new String(in.read(in.readInt())), (o,v)->writeString(o,(String)v,4)),
        FLOAT4((in)->in.readFloat(), (o,v)->o.writeFloat(((Number) v).floatValue())),
        FLOAT8((in)->in.readDouble(), (o,v)->o.writeDouble(((Number) v).doubleValue())),
        INT1((in)->in.read(), (o,v)->o.write(((Number) v).byteValue())),
        INT2((in)->in.readShort(), (o,v)->o.writeShort(((Number) v).shortValue())),
        INT4((in)->in.readInt(), (o,v)->o.writeInt(((Number) v).intValue())),
        INT8((in)->in.readLong(), (o,v)->o.writeLong(((Number) v).longValue())),
        BOOLEAN((in)->in.read() > 0 ? true: false, (o,v)->o.write((byte)(((Boolean) v) ? 1 : 0))),
        NULL((in)->null, (o,v)->doNothing());

        final ReadObjectValue read;
        final WriteObjectValue write;

        ValueType(ReadValue read, WriteValue write) {
            this.read = read;
            this.write = write;
        }
        ValueType(ReadObjectValue read, WriteObjectValue write) {
            this.read = read;
            this.write = write;
        }

        byte byteValue() {
            return ((byte) ordinal());
        }

        public Object read(LongBiIndex<String> fields, InputBuffer in) throws IOException {
            return read.read(fields, in);
        }

        public void write(LongBiIndex<String> fields, OutputBuffer out, Object value) throws IOException {
            write.write(fields, out, value);
        }
    }

    final LongBiIndex<String> fields;

    public BsonFormat(LongBiIndex<String> fields) {
        this.fields = fields;
    }

    @Override
    public DTO read(InputStream in) throws IOException {
        final InputBuffer inputBuffer = new InputBuffer(in);
        final DTO object = readObject(fields, inputBuffer, inputBuffer.readInt());
        return object;
    }

    private static DTO readObject(LongBiIndex<String> fields, InputBuffer in, int objectSize) throws IOException {
        final List<Pair<String, Object>> assignments = new ArrayList<>(objectSize);
        for (int i=0; i < objectSize; i++) {
            final short fieldIndex = in.readShort();
            final String key = fields.get(fieldIndex);
            final int typeIndex = in.read();
            final ValueType type = ValueType.values()[typeIndex];
            final Object value = type.read(fields, in);
            assignments.add(new Pair<>(key, value));
        }
        return new ListDTO(assignments);
    }

    private static Collection readArray(LongBiIndex<String> fields, InputBuffer in, int arraySize) throws IOException {
        Collection array = new ArrayList<>(arraySize);
        final int typeIndex = in.read();
        final ValueType type = ValueType.values()[typeIndex];
        for (int i=0; i < arraySize; i++)
            array.add(type.read(fields, in));
        return array;
    }

    @Override
    public void write(DTO object, OutputStream out) {
        try {
            final OutputBuffer outputBuffer = new OutputBuffer(out);
            writeObject(fields, outputBuffer, object);
            outputBuffer.flush();
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }

    private static void writeObject(LongBiIndex<String> fields, OutputBuffer out, DTO object) throws IOException {
        out.writeInt(object.size());
        for (Pair<String, Object> assignment : object) {
            out.writeShort((short) fields.get(assignment.getKey()));
            final ValueType type = getValueType(assignment.getValue());
            out.write((byte) type.ordinal());
            type.write(fields, out, assignment.getValue());
        }
    }

    private static void writeCollection(LongBiIndex<String> fields, OutputBuffer out, Collection collection) throws IOException {
        final Iterator<Object> values = collection.iterator();
        if (!values.hasNext())
            out.writeInt(ValueType.NULL.ordinal());
        final Object first = values.next();
        final ValueType elemType = getValueType(first);
        out.write(elemType.byteValue());

        elemType.write(fields, out, first);
        while (values.hasNext())
            elemType.write(fields, out, values.next());
    }

    private static void writeString(OutputBuffer out, String string, int nbytes) throws IOException {
        final byte[] bytes = string.getBytes();
        switch (nbytes) {
            case 1:
                out.write((byte) bytes.length);
                break;
            case 2:
                out.writeShort((short) bytes.length);
                break;
            case 4:
                out.writeInt(bytes.length);
                break;
        }
        out.write(bytes);
    }

    private static ValueType getValueType(Object value) throws IOException {
        if (value == null)
            return ValueType.NULL;

        else if (value instanceof Collection && arrayLessThan(value, Byte.MAX_VALUE))
            return ValueType.ARRAY1;

        else if (value instanceof Collection && arrayLessThan(value, Short.MAX_VALUE))
            return ValueType.ARRAY2;

        else if (value instanceof Collection)
            return ValueType.ARRAY4;

        else if (value instanceof String && stringLessThan(value, Byte.MAX_VALUE))
            return ValueType.STRING1;

        else if (value instanceof String && stringLessThan(value, Short.MAX_VALUE))
            return ValueType.STRING2;

        else if (value instanceof String)
            return ValueType.STRING4;

        else if (value instanceof DTO)
            return ValueType.OBJECT4;

        else if (value instanceof Byte || lessThan(value, Byte.MAX_VALUE))
            return ValueType.INT1;

        else if (value instanceof Short || lessThan(value, Short.MAX_VALUE))
            return ValueType.INT2;

        else if (value instanceof Integer || lessThan(value, Integer.MAX_VALUE))
            return ValueType.INT4;

        else if (value instanceof Long)
            return ValueType.INT8;

        else if (value instanceof Float)
            return ValueType.FLOAT4;

        else if (value instanceof Double)
            return ValueType.FLOAT8;

        else if (value instanceof Boolean)
            return ValueType.BOOLEAN;

        throw new IllegalArgumentException("Unknown value type for "+value.getClass().getName());
    }

    private static boolean stringLessThan(Object value, int comparison) {
        return ((String) value).length() < comparison;
    }
    private static boolean arrayLessThan(Object value, int comparison) {
        return ((Collection) value).size() < comparison;
    }
    private static boolean lessThan(Object value, long comparison) {
        if (!(value instanceof Number) || value instanceof Float || value instanceof Double)
            return false;
        return ((Number) value).longValue() < comparison;
    }

    private static void doNothing() {}


}
