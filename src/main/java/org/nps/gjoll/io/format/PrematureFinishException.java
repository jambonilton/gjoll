package org.nps.gjoll.io.format;

import org.nps.gjoll.io.SerializationException;

public class PrematureFinishException extends SerializationException {

    private static final long serialVersionUID = 1L;

    public PrematureFinishException(int index, char expected) {
        super(String.format("Unexpected end of input at index %d. Expected %c.", index, (char) expected));
    }
    
}