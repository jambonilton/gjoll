package org.nps.gjoll.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * Interface for wrapping InputStream.
 * TODO what's the point of this thing?
 */
public interface Input extends AutoCloseable {

    int read() throws IOException;

    String readString(int count) throws IOException;

    void close() throws IOException;
    
    InputStream getInputStream();

}