package org.nps.gjoll;

import org.nps.gjoll.events.Change;
import org.nps.gjoll.events.StandardChangeFormat;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.main.DataStoreBuilder;
import org.nps.gjoll.net.requests.DataStoreServerRunner;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.StandardQueryFormat;
import org.nps.gjoll.std.MapDataStoreRegistry;
import org.nps.gjoll.updates.StandardUpdateFormat;
import org.nps.gjoll.updates.Update;

public class Gjoll {

    /**
     * Static query format holders for convenience methods in DataStore and Update interfaces.
     */
    private static Format<Query> queryFormat = new StandardQueryFormat();
    private static Format<Update> updateFormat = new StandardUpdateFormat(queryFormat);
    private static Format<Change> changeFormat = new StandardChangeFormat();
    private static DataStoreRegistry registry = new MapDataStoreRegistry();

    public static Format<Query> getQueryFormat() {
        return queryFormat;
    }

    public static void setQueryFormat(Format<Query> queryFormat) {
        Gjoll.queryFormat = queryFormat;
    }

    public static Format<Update> getUpdateFormat() {
        return updateFormat;
    }

    public static void setUpdateFormat(Format<Update> updateFormat) {
        Gjoll.updateFormat = updateFormat;
    }

    public static Format<Change> getChangeFormat() { return changeFormat; }

    public static void setChangeFormat(Format<Change> changeFormat) { Gjoll.changeFormat = changeFormat; }

    public static void broadcastOnPort(int port) {
        DataStoreServerRunner.runServer(registry, port);
    }

    /**
     * Get a datastore that will be set in the global registry under the given name.
     * @param name
     * @return
     */
    public static DataStoreBuilder newDataStore(String name) {
        return new DataStoreBuilder().setName(name).setRegistry(registry);
    }

    /**
     * Create an unnamed data store.
     * @return
     */
    public static DataStoreBuilder newDataStore() {
        return new DataStoreBuilder();
    }

    public static DataStore getDataStore(String name) {
        return registry.get(name);
    }
}
