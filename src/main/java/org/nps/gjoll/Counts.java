package org.nps.gjoll;

import org.nps.gjoll.query.Pair;

public interface Counts extends Iterable<Pair<Object, Long>> {
    Long get(Object key);
}
