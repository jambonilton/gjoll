package org.nps.gjoll.main;

import org.nps.gjoll.DataStore;
import org.nps.gjoll.DataStoreRegistry;
import org.nps.gjoll.Gjoll;
import org.nps.gjoll.events.DataPublishingStore;
import org.nps.gjoll.index.*;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.format.JsonFormat;
import org.nps.gjoll.log.LoggingDataStore;
import org.nps.gjoll.log.PrintStreamLogWriter;
import org.nps.gjoll.net.RemoteDataStore;
import org.nps.gjoll.std.AuditableDataStore;
import org.nps.gjoll.std.FileStore;
import org.nps.gjoll.std.ListStore;
import org.nps.gjoll.std.UpdateStoreImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.time.Clock;
import java.util.ArrayList;
import java.util.List;

/**
 * Builder responsible for convenient structuring of data stores without all the hassle of
 * using the "new" keyword, or having to remember class names or whatnot.
 */
public class DataStoreBuilder {
    
    private PrintStream logOut = System.out;
    private boolean loggingEnabled = true;
    private Format format = new JsonFormat();
    private List<Index> indices = new ArrayList<>();
    private String dataFile;
    private DataStoreRegistry registry;
    private String name;
    private String remoteHost;
    private Integer remotePort;

    //================================ Registry ================================//

    /**
     * Registers this data store under the specified name when built.
     * @param name
     * @return
     */
    public DataStoreBuilder setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Registers this data store under the specified registry when built.
     * @param registry
     * @return
     */
    public DataStoreBuilder setRegistry(DataStoreRegistry registry) {
        this.registry = registry;
        return this;
    }

    //================================ Storage ================================//

    /**
     * Sets the storage file for this data store.  This option is mutually exclusive
     * with the remote data store option.
     * @param dataFile
     * @return
     */
    public DataStoreBuilder setDataFile(String dataFile) {
        this.dataFile = dataFile;
        return this;
    }

    /**
     * Sets the output for objects for storage if this is a file store.
     * @param format
     * @return
     */
    public DataStoreBuilder setFormat(Format format) {
        this.format = format;
        return this;
    }

    //================================ Logging ================================//

    /**
     * Sets the logging to output to the given file.  If it fails to open the file, it will print a
     * stack trace and log to standard out.
     * @param file
     * @return
     */
    public DataStoreBuilder setLogFile(File file) {
        if (isRemoteStore())
            throw new IllegalStateException("Cannot set data file after setting remote host!");
        try {
            this.setLogOutput(new PrintStream(new FileOutputStream(file, true)));
        } catch (IOException e) {
            System.err.println("Failed to initialize logger for file "+file+".  Using stdout instead.");
            e.printStackTrace();
        }
        return this;
    }

    /**
     * Sets the output for the logging.
     * @param out
     * @return
     */
    public DataStoreBuilder setLogOutput(PrintStream out) {
        this.logOut = out;
        return this;
    }

    /**
     * Turns off all logging for this data store.
     * @return
     */
    public DataStoreBuilder disableLogging() {
        this.loggingEnabled = false;
        return this;
    }

    //================================ Networking ================================//

    /**
     * All calls to this data store will be submitted to the target host.
     * @param host
     * @param port
     * @return
     */
    public DataStoreBuilder connect(String host, Integer port) {
        if (isFileStore())
            throw new IllegalStateException("Cannot set remote host after setting the data file!");
        this.remoteHost = host;
        this.remotePort = port;
        return this;
    }

    //================================= Indexing =================================//

    /**
     * Indexes the given field for partial text searching.
     * @param field
     * @return
     */
    public DataStoreBuilder addStringContainsIndex(String field) {
        this.indices.add(new ContainsIndex(field, new RegexStringItemizer()));
        return this;
    }

    /**
     * Indexes the given field for finding items in a collection.
     * @param field
     * @return
     */
    public DataStoreBuilder addSetContainsIndex(String field) {
        this.indices.add(new ContainsIndex(field, new CollectionItemizer()));
        return this;
    }

    /**
     * Indexes the given field for comparisons.  Values for the field MUST implement
     * Comparable (i.e. strings, integers, etc).
     * @param field
     * @return
     */
    public DataStoreBuilder addTreeIndex(String field) {
        this.indices.add(new TreeIndex<>(field));
        return this;
    }

    //================================ Build ================================//

    public DataStore build() {
        try {
            DataStore store = buildStore();
            if (loggingEnabled)
                store = addLogging(store);
            if (isRegistered())
                registry.set(name, store);
            return store;
        } catch (IOException e) {
            throw new DataStoreBuilderException(e);
        }
    }

    private boolean isRegistered() {
        return registry != null && name != null;
    }

    private DataStore buildStore() throws IOException {
        if (isFileStore())
            return auditableFileStore();
        else if (isRemoteStore())
            return remoteDataStore();
        else
            return new DataPublishingStore<>(new ListStore());
    }
    private boolean isRemoteStore() {
        return remoteHost != null;
    }

    private boolean isFileStore() {
        return dataFile != null;
    }

    private DataStore auditableFileStore() throws IOException {
		final FileStore fileStore = new FileStore(dataFile, format),
						updateStore = new FileStore(dataFile+".updates", format);
		return new AuditableDataStore(indexed(fileStore), new UpdateStoreImpl(updateStore));
	}

    private DataStore remoteDataStore() {
        return new RemoteDataStore(name, remoteHost, remotePort, this.format, Gjoll.getQueryFormat(), Gjoll.getUpdateFormat());
    }


    private RandomAccessStore indexed(RandomAccessStore store) {
		if (indices.isEmpty())
			return store;
		else if (indices.size() == 1)
			return new IndexedDataStore(indices.get(0), store);
		// TODO index everything?
		return new IndexedDataStore(new MultiIndex(indices), store);
	}

    private LoggingDataStore<DataStore> addLogging(DataStore store) {
        return new LoggingDataStore<>(store, 
                        makeLogger(store.getClass().getSimpleName()),
                        format,
                        Gjoll.getQueryFormat(),
                        Gjoll.getUpdateFormat());
    }

    private PrintStreamLogWriter makeLogger(final String name) {
        return new PrintStreamLogWriter(name, logOut, Clock.systemDefaultZone());
    }
    
    public static class DataStoreBuilderException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        public DataStoreBuilderException(Throwable cause) {
            super(cause);
        }
        
    }
}
