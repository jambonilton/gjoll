package org.nps.gjoll;

import org.nps.gjoll.events.Change;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.updates.Update;

import java.util.function.Consumer;
import java.util.stream.Stream;

public class TypedStore<E> implements Store<E> {

    final DataStore base;
    final Converter<DTO, E> converter;

    public TypedStore(DataStore base, Converter<DTO, E> converter) {
        this.base = base;
        this.converter = converter;
    }

    @Override
    public void add(E item) {
        base.add(converter.from(item));
    }

    @Override
    public void add(Stream<E> stream) {
        base.add(stream.map(converter::from));
    }

    @Override
    public long size() {
        return base.size();
    }

    @Override
    public Stream<E> get() {
        return base.get().map(converter::to);
    }

    @Override
    public Stream<E> get(String str) {
        return base.get(str).map(converter::to);
    }

    @Override
    public Stream<E> get(Query query) {
        return base.get(query).map(converter::to);
    }

    @Override
    public Counts counts(String query, String groupBy) {
        return base.counts(query, groupBy);
    }

    @Override
    public Counts counts(Query query, String groupBy) {
        return base.counts(query, groupBy);
    }

    @Override
    public void apply(Update update) {
        base.apply(update);
    }

    @Override
    public void onChange(Query criteria, Consumer<Change> action) {
        base.onChange(criteria, action);
    }
}
