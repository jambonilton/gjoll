package org.nps.gjoll.events;

import java.util.function.Consumer;

import org.nps.gjoll.query.Query;

public class DataSubscriber {
    
    final Query query;
    final Consumer<Change> action;
    
    public DataSubscriber(Query query, Consumer<Change> action) {
        this.query = query;
        this.action = action;
    }
    
    public Query query() {
        return query;
    }
    
    public Consumer<Change> action() {
        return action;
    }
    
}