package org.nps.gjoll.events;

import org.nps.gjoll.Counts;
import org.nps.gjoll.DTO;
import org.nps.gjoll.DataSource;
import org.nps.gjoll.DataStore;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.updates.MutableDataSink;
import org.nps.gjoll.updates.Update;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class DataPublishingStore<S extends DataSource & MutableDataSink> implements DataStore {

    final S base;
    final Collection<DataSubscriber> subscribers;

    public DataPublishingStore(S base) {
        this(base, new ArrayList<>());
    }

    public DataPublishingStore(S base, Collection<DataSubscriber> subscribers) {
        this.base = base;
        this.subscribers = subscribers;
    }

    public void add(DTO item) {
        publish(new InsertChange(item));
        base.add(item);
    }

    public void apply(Update update) {
        base.get(update.where()).map((e)->new UpdateChange(e, update)).forEach(this::publish);
        base.apply(update);
    }

    public long size() {
        return base.size();
    }

    public Stream<DTO> get() {
        return base.get();
    }

    public Stream<DTO> get(String str) {
        return base.get(str);
    }

    public Stream<DTO> get(Query query) {
        return base.get(query);
    }

    public Counts counts(String query, String groupBy) {
        return base.counts(query, groupBy);
    }

    public Counts counts(Query query, String groupBy) {
        return base.counts(query, groupBy);
    }

    @Override
    public void onChange(Query criteria, Consumer<Change> action) {
        this.subscribers.add(new DataSubscriber(criteria, action));
    }
    
    private void publish(final Change change) {
        subscribers.stream().filter((s)->s.query.test(change.item())).forEach((s)->s.action.accept(change));
    }

}
