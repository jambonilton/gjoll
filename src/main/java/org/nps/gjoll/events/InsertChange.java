package org.nps.gjoll.events;

import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Pair;

public class InsertChange implements Change {

    final DTO item;
    
    public InsertChange(DTO item) {
        this.item = item;
    }

    @Override
    public ChangeType type() {
        return ChangeType.INSERT;
    }

    @Override
    public DTO item() {
        return item;
    }

    @Override
    public Iterable<Pair<String, Object>> assignments() {
        return item;
    }
    
}