package org.nps.gjoll.events;

import java.util.Collection;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.nps.gjoll.log.LogWriter;
import org.nps.gjoll.log.PrintStreamLogWriter;

// TODO kill me
public class EventStream<E> {

    private static final int BUFFER_SIZE = 50;
    
    final LogWriter log;
    final Collection<Consumer<E>> subscribers;
    final BlockingQueue<E> queue;
    
    public EventStream() {
        this(new PrintStreamLogWriter(EventStream.class.getName()));
    }

    public EventStream(LogWriter log) {
        this.log = log;
        this.subscribers = new Vector<>();
        this.queue = new ArrayBlockingQueue<>(BUFFER_SIZE);
        Executors.newSingleThreadExecutor().execute(new EventLoop());
    }
    
    public void publish(E event) {
        queue.add(event);
    }
    
    public void subscribe(Consumer<E> consumer) {
        subscribers.add(consumer);
    }
    
    public Stream<E> stream() {
        final StreamConsumer<E> consumer = new StreamConsumer<>();
        subscribers.add(consumer);
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(consumer.iterator(), Spliterator.IMMUTABLE), false);
    }
    
    class EventLoop implements Runnable {

        @Override
        public void run() {
            while (true)
                consume();
        }

        private void consume() {
            try {
                E e = queue.take();
                for (Consumer<E> s : subscribers)
                    consume(s, e);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        private void consume(Consumer<E> subscriber, E event) {
            try {
                subscriber.accept(event);
            } catch (RuntimeException e) {
                log.error("Failed to consume event. Removing subscriber.", e);
                subscribers.remove(subscriber);
            }
        }
        
    }
    
    static class StreamConsumer<E> implements Consumer<E>, Iterable<E> {

        final BlockingQueue<E> queue;
        
        public StreamConsumer() {
            this.queue = new ArrayBlockingQueue<>(BUFFER_SIZE);
        }

        @Override
        public void accept(E e) {
            queue.add(e);
        }

        @Override
        public Iterator<E> iterator() {
            return new UnendingBlockingQueueIterator<>(queue);
        }
        
    }
    
    static class UnendingBlockingQueueIterator<E> implements Iterator<E> {

        final BlockingQueue<E> queue;
        
        public UnendingBlockingQueueIterator(BlockingQueue<E> queue) {
            this.queue = queue;
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public E next() {
            try {
                return queue.take();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return null;
            }
        }
        
    }
    
}
