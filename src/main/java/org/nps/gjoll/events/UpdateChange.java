package org.nps.gjoll.events;

import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Pair;
import org.nps.gjoll.updates.Update;

public class UpdateChange extends InsertChange {
    
    final Update update;

    public UpdateChange(DTO item, Update update) {
        super(item);
        this.update = update;
    }

    @Override
    public ChangeType type() {
        return ChangeType.UPDATE;
    }

    @Override
    public DTO item() {
        return item;
    }

    @Override
    public Iterable<Pair<String, Object>> assignments() {
        return update.assignments(item());
    }
    
}