package org.nps.gjoll.events;

import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Pair;

/**
 * Represents a change to an object.  Holds a reference to the object changed
 * and the assignments that were made.
 */
public interface Change {

    /**
     * Either INSERT or UPDATE.
     * TODO add delete?
     */
    ChangeType type();

    /**
     * The object that was changed.
     */
    DTO item();

    /**
     * The changes that were made, in the form of key-value pairs.  For inserts, this will
     * just return all the fields of the object.
     */
    Iterable<Pair<String, Object>> assignments();
    
    enum ChangeType {
        INSERT, UPDATE
    }
    
}
