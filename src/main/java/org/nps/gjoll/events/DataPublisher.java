package org.nps.gjoll.events;

import java.util.function.Consumer;

import org.nps.gjoll.query.Query;

public interface DataPublisher {

    void onChange(Query criteria, Consumer<Change> action);
    
}
