package org.nps.gjoll.events;

import org.nps.gjoll.Gjoll;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.IOUtils;
import org.nps.gjoll.io.PeekingInputStream;
import org.nps.gjoll.io.SerializationException;
import org.nps.gjoll.io.format.JsonFormat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Standard format for changes.  Uses JSON format to serialize updated
 * objects as well as assignments.
 */
public class StandardChangeFormat implements Format<Change> {

    @Override
    public void write(Change change, OutputStream out) {
        try {
            out.write((change.type().name()+'\n').getBytes());
            JsonFormat.writeObject(change.item(), out);
            if (change.type().equals(Change.ChangeType.UPDATE))
                JsonFormat.writeObject(change.assignments(), out);
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }

    @Override
    public Change read(InputStream in) throws IOException {
        final Change.ChangeType type = Change.ChangeType.valueOf(IOUtils.readLine(in));
        switch (type) {
            case INSERT:
                return readInsert(in);
            case UPDATE:
                return readUpdate(in);
        }
        throw new IllegalArgumentException(type + " not supported!");
    }

    private Change readInsert(InputStream in) throws IOException {
        return new InsertChange(JsonFormat.readObject(new PeekingInputStream(in)));
    }

    private Change readUpdate(InputStream in) throws IOException {
        final PeekingInputStream peekingIn = new PeekingInputStream(in);
        return new UpdateChange(JsonFormat.readObject(peekingIn), Gjoll.getUpdateFormat().read(peekingIn.getInputStream()));
    }


}
