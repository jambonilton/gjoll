package org.nps.gjoll.reflect;

public class Reflect {

    @SuppressWarnings("unchecked")
    public static <T> T cast(Object value) {
        if (value == null)
            return null;
        return (T) value;
    }
    
}
