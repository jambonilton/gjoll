package org.nps.gjoll.reflect;

import org.nps.gjoll.query.Pair;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

public class QueryableReflector {


    static Map<Class<?>, QueryableReflector> map = new HashMap<>();

    public static <T> QueryableReflector reflect(T instance) {
        final Class<?> type = instance.getClass();
        if (map.containsKey(type))
            return map.get(type);
        final QueryableReflector newValue = create(type);
        map.put(type, newValue);
        return newValue;
    }

    private static <T> QueryableReflector create(Class<T> type) {
        final Map<String, Field> fields = new HashMap<>();
        for (final Field field : type.getDeclaredFields()) {
            if (Modifier.isStatic(field.getModifiers()))
                continue;
            field.setAccessible(true);
            fields.put(field.getName(), field);
        }
        return new QueryableReflector(fields);
    }

    final Map<String, Field> fields;

    public QueryableReflector(Map<String, Field> fields) {
        this.fields = fields;
    }

    public int size() {
        return fields.size();
    }

    public Object get(Object instance, String field) {
        final Field f = fields.get(field);
        if (f == null)
            return null;
        return getFieldValue(instance, f);
    }

    public Collection<String> fields() {
        return fields.keySet();
    }

    public Collection<Object> values(Object instance) {
        return fields.values().stream().map((f)-> getFieldValue(instance, f)).collect(Collectors.toList());
    }

    public Iterator<Pair<String, Object>> pairs(Object instance) {
        return fields.entrySet().stream().map((e)->new Pair<>(e.getKey(), getFieldValue(instance, e.getValue()))).iterator();
    }

    private Object getFieldValue(Object instance, Field f) {
        try {
            return f.get(instance);
        } catch (ReflectiveOperationException e) {
            return null;
        }
    }

}
