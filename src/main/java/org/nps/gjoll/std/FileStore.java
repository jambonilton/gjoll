package org.nps.gjoll.std;

import org.nps.gjoll.DTO;
import org.nps.gjoll.index.RandomAccessStore;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.query.Query;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.PrimitiveIterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Basic read-only file store.
 */
public class FileStore implements RandomAccessStore {

    final File file;
    final FileOutputStream out;
    final Format<DTO> format;
    
    long size;

    public FileStore(String path, Format<DTO> format) throws IOException {
        this(new File(path), format);
    }
    
    public FileStore(File file, Format<DTO> format) throws IOException {
        this(file, new FileOutputStream(file, true), format);
    }

    FileStore(File file, 
              FileOutputStream out,
              Format<DTO> format) {
        this.file = file;
        this.out = out;
        this.format = format;
        size = file.length() == 0 ? 0 : get().count();
    }

    @Override
    public void add(DTO item) {
        try {
            format.write(item, out);
            out.flush();
            size++;
        } catch (IOException e) {
            throw new FileStoreException(e);
        }
    }

    @Override
    public long size() {
        return size;
    }

    @Override
    public Stream<DTO> get() {
        try {
            return format.stream(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new FileStoreException(e);
        }
    }

    @Override
    public DTO get(long key) {
        try {
            final FileInputStream in = new FileInputStream(file);
            in.skip(key);
            return format.read(in);
        } catch (IOException e) {
            throw new FileStoreException(e);
        }
    }

    @Override
    public long addAndLocate(DTO item) {
        final long location = file.length();
        add(item);
        return location;
    }
    
    @Override
    public LongStream getIndices(Query q) {
    	return StreamSupport.longStream(Spliterators.spliteratorUnknownSize(new FileIndexStream(q), Spliterator.DISTINCT), false);
    }
    
    final class FileIndexStream implements PrimitiveIterator.OfLong {

    	final FileInputStream in;
    	final FileChannel fc;
    	final Query query;
    	
    	long index;
		DTO obj;
    	
		public FileIndexStream(Query query) {
			try {
				this.in = new FileInputStream(file);
				this.fc = in.getChannel();
				this.query = query;
				seekNext();
			} catch (IOException e) {
				throw new FileStoreException(e);
			}
		}
		
		private void seekNext() {
			advance();
			while (obj != null && !query.test(obj))
				advance();
		}

		private void advance() {
			try {
				index = fc.position();
				obj = format.read(in);
			} catch (IOException e) {
				throw new FileStoreException(e);
			}
		}

		@Override
		public boolean hasNext() {
			return obj != null;
		}
		
		@Override
		public long nextLong() {
			final long next = index;
			seekNext();
			return next;
		}
    	
    }

    public static class FileStoreException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        public FileStoreException(Throwable cause) {
            super(cause);
        }
        
    }
    
    public static class FileStoreWriteException extends FileStoreException {

        private static final long serialVersionUID = 1L;

        public FileStoreWriteException(Throwable cause) {
            super(cause);
        }
  
    }
    
}
