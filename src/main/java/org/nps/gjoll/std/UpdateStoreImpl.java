package org.nps.gjoll.std;

import org.nps.gjoll.Gjoll;
import org.nps.gjoll.DTO;
import org.nps.gjoll.index.*;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.format.ListDTO;
import org.nps.gjoll.updates.Update;

import java.util.stream.Stream;

import static org.nps.gjoll.query.Query.contains;

/**
 * Piggy-backs on general storage format, using two fields to encode the affected IDs and the update
 * expression itself.
 */
public class UpdateStoreImpl implements UpdateStore {

    private static final String IDS_FIELD = "$update_ids", UPDATE_FIELD = "$update";
    
    final RandomAccessStore store;
    final Format<Update> updateFormat = Gjoll.getUpdateFormat();
    
    public UpdateStoreImpl(RandomAccessStore store) {
        final Index index = new ContainsIndex(IDS_FIELD, new CollectionItemizer());
        this.store = new IndexedDataStore(index, store);
    }

    @Override
    public void add(Update update, long[] indices) {
        store.add(toObject(update, indices));
    }

    private ListDTO toObject(Update update, long[] indices) {
        return new ListDTO()
                .put(IDS_FIELD, indices)
                .put(UPDATE_FIELD, updateFormat.toString(update));
    }

    @Override
    public Stream<Update> get(long id) {
        return store.get(contains(IDS_FIELD, id)).map(this::toUpdate);
    }
    
    private Update toUpdate(DTO dto) {
        return updateFormat.read((String) dto.get(UPDATE_FIELD));
    }
    
}
