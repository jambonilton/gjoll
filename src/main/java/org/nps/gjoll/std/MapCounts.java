package org.nps.gjoll.std;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.nps.gjoll.Counts;
import org.nps.gjoll.query.Pair;

public class MapCounts implements Counts {

    final Map<Object, Long> counts;

    public MapCounts() {
        this.counts = new HashMap<Object, Long>();
    }
    
    public MapCounts(Map<Object, Long> counts) {
        this.counts = counts;
    }

    @Override
    public Iterator<Pair<Object, Long>> iterator() {
        return counts.entrySet().stream().map((e)->new Pair<Object,Long>(e.getKey(), e.getValue())).iterator();
    }

    @Override
    public Long get(Object key) {
        final Long count = counts.get(key);
        if (count == null)
            return 0L;
        return count;
    }

    public void increment(Object key) {
        add(key, 1L);
    }
    
    public void put(Object key, Long count) {
        counts.put(key, count);
    }
    
    public void add(Object key, Long add) {
        final Long count = counts.get(key);
        if (count == null)
            counts.put(key, add);
        else
            counts.put(key, count+add);
    }

}
