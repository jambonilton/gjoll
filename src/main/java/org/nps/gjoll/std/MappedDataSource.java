package org.nps.gjoll.std;

import org.nps.gjoll.DataSource;
import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Query;

import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class MappedDataSource implements DataSource {

    final DataSource source;
    final UnaryOperator<DTO> map;

    public MappedDataSource(DataSource source, UnaryOperator<DTO> map) {
        this.source = source;
        this.map = map;
    }

    @Override
    public Stream<DTO> get(Query query) {
        return source.get(query.ignoreEmptyParameters())
                .map(map)
                .filter(query);
    }

    @Override
    public Stream<DTO> get() {
        return source.get().map(map);
    }

    @Override
    public long size() {
        return source.size();
    }

}
