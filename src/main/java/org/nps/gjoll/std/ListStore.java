package org.nps.gjoll.std;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.nps.gjoll.DTO;
import org.nps.gjoll.index.RandomAccessStore;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.streams.IndexListIterator;
import org.nps.gjoll.updates.MutableDataSink;
import org.nps.gjoll.updates.Update;

/**
 * Simplest data source of them all.  Holds all the objects in an in-memory list.
 */
public class ListStore implements RandomAccessStore, MutableDataSink {

    final List<DTO> list;
    
    public ListStore() {
        this(new ArrayList<>());
    }

    public ListStore(List<DTO> bucket) {
        this.list = bucket;
    }

    @Override
    public Stream<DTO> get() {
        return list.stream();
    }

    @Override
    public DTO get(long key) {
        return list.get((int) key);
    }

    @Override
    public void add(DTO item) {
        list.add(item);
    }

    @Override
    public long size() {
        return Long.valueOf(list.size());
    }

    @Override
    public long addAndLocate(DTO item) {
        add(item);
        return list.size()-1;
    }

    @Override
    public void apply(Update update) {
        for (int i=0; i < this.list.size(); i++)
            list.set(i, update.apply(list.get(i)));
    }

	@Override
	public LongStream getIndices(Query q) {
		return IndexListIterator.stream(list).mapToLong((p)->p.getKey().longValue());
	}
    
    
    
}
