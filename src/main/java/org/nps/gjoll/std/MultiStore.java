package org.nps.gjoll.std;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.nps.gjoll.ReadOnlyStore;
import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Query;

public class MultiStore implements ReadOnlyStore {
    
    final List<ReadOnlyStore> stores;
    final BiFunction<List<ReadOnlyStore>, DTO, ReadOnlyStore> insertionSelector;
    final Function<Query, Predicate<ReadOnlyStore>> querySelector;

    public MultiStore(List<ReadOnlyStore> stores) {
        this(stores, new RoundRobinDataStoreSelector(), (q)->(d)->true);
    }

    public MultiStore(
            List<ReadOnlyStore> stores,
            BiFunction<List<ReadOnlyStore>, DTO, ReadOnlyStore> insertionSelector,
            Function<Query, Predicate<ReadOnlyStore>> querySelector) {
        this.stores = stores;
        this.insertionSelector = insertionSelector;
        this.querySelector = querySelector;
    }

    @Override
    public Stream<DTO> get() {
        return stores.parallelStream().map(ReadOnlyStore::get).reduce(Stream::concat).get();
    }

    @Override
    public Stream<DTO> get(Query query) {
        return stores.parallelStream().map((s)->s.get(query)).reduce(Stream::concat).get();
    }

    @Override
    public void add(DTO item) {
        insertionSelector.apply(stores, item).add(item);
    }
    
    public static class RoundRobinDataStoreSelector implements BiFunction<List<ReadOnlyStore>, DTO, ReadOnlyStore> {

        int index=0;
        
        @Override
        public ReadOnlyStore apply(List<ReadOnlyStore> list, DTO item) {
            final ReadOnlyStore source = list.get(index);
            index=(index+1) % list.size();
            return source;
        }
        
    }

    @Override
    public long size() {
        return stores.stream().mapToLong(ReadOnlyStore::size).sum();
    }
    
}
