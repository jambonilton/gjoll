package org.nps.gjoll.std;

import org.nps.gjoll.DTO;
import org.nps.gjoll.DataStore;
import org.nps.gjoll.DataStoreRegistry;

import java.util.HashMap;
import java.util.Map;

public class MapDataStoreRegistry implements DataStoreRegistry {

    final Map<String, DataStore> map;

    public MapDataStoreRegistry() {
        this(new HashMap<>());
    }

    public MapDataStoreRegistry(Map<String, DataStore> map) {
        this.map = map;
    }

    @Override
    public DataStore get(String name) {
        return map.get(name);
    }

    @Override
    public void set(String name, DataStore store) {
        map.put(name, store);
    }
}
