package org.nps.gjoll.std;

import org.nps.gjoll.DTO;
import org.nps.gjoll.query.Pair;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

public class ArrayDTO implements DTO {
    
    private static final long serialVersionUID = 1L;
    
    final Map<String,Integer> fieldIndex;
    final Object[] row;
    
    public ArrayDTO(Map<String, Integer> fieldIndex, Object[] row) {
        this.fieldIndex = fieldIndex;
        this.row = row;
    }
    
    @Override
    public Iterator<Pair<String, Object>> iterator() {
        return fieldIndex.entrySet().stream().map((e)->new Pair<String,Object>(e.getKey(), row[e.getValue()])).iterator();
    }
    
    @Override
    public Object get(String field) {
        final Integer i = fieldIndex.get(field);
        if (i == null)
            throw new IllegalArgumentException("Missing field \""+field+"\"");
        return row[i];
    }
    
    @Override
    public Collection<String> fields() {
        return fieldIndex.keySet().stream().sorted((k1,k2)->fieldIndex.get(k1).compareTo(fieldIndex.get(k2))).collect(Collectors.toList());
    }
    
    @Override
    public Collection<Object> values() {
        return Arrays.asList(row);
    }

    @Override
    public int size() {
        return row.length;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        final Iterator<Pair<String,Object>> iter = iterator();
        sb.append(iter.next());
        while (iter.hasNext())
            sb.append(", ").append(iter.next());
        return sb.toString();
    }
}
