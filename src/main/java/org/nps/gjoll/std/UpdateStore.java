package org.nps.gjoll.std;

import java.util.function.Function;
import java.util.stream.Stream;

import org.nps.gjoll.DTO;
import org.nps.gjoll.updates.Update;

public interface UpdateStore {

    void add(Update update, long[] result);
    
    Stream<Update> get(long id);
    
    default Function<DTO,DTO> getAsFunction(long id) {
        return get(id).map((u)->(Function<DTO,DTO>) u).reduce(Function::andThen).orElse(Function.identity());
    }

}
