package org.nps.gjoll.std;

import org.nps.gjoll.DTO;
import org.nps.gjoll.DataStore;
import org.nps.gjoll.events.Change;
import org.nps.gjoll.events.DataSubscriber;
import org.nps.gjoll.events.InsertChange;
import org.nps.gjoll.events.UpdateChange;
import org.nps.gjoll.index.RandomAccessStore;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.updates.Update;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * Standard implementation for the main DataStore interface.
 */
public class AuditableDataStore implements DataStore {

    final RandomAccessStore base;
    final UpdateStore updates;
    final Collection<DataSubscriber> subscribers;

    public AuditableDataStore(RandomAccessStore base, UpdateStore updates) {
        this.base = base;
        this.updates = updates;
        this.subscribers = new ArrayList<>();
    }

    @Override
    public void add(DTO item) {
        publish(new InsertChange(item));
        base.add(item);
    }

    @Override
    public void apply(Update update) {
    	final long[] indices = base.getIndices(update.where()).toArray();
		LongStream.of(indices).forEach((i)->publish(new UpdateChange(base.get(i), update)));
        updates.add(update, indices);
    }

    @Override
    public long size() {
        return base.size();
    }

    @Override
    public Stream<DTO> get() {
        return get(Query.alwaysTrue());
    }
    
    @Override
    public Stream<DTO> get(Query query) {
        return base.getIndices(query).mapToObj(this::getUpdated);
    }
    
    private DTO getUpdated(long key) {
        return updates.getAsFunction(key).apply(base.get(key));
    }

    @Override
    public void onChange(Query criteria, Consumer<Change> action) {
        subscribers.add(new DataSubscriber(criteria, action));
    }
    
    private void publish(final Change change) {
        subscribers.stream().filter((s)->s.query().test(change.item())).forEach((s)->s.action().accept(change));
    }
    
}
