package org.nps.gjoll.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.SerializationException;

public class LongFormat implements Format<Long> {

    private static LongFormat INSTANCE = new LongFormat();
    
    public static LongFormat get() {
        return INSTANCE;
    }
    
    @Override
    public void write(Long l, OutputStream out) {
        try {
            out.write(ByteBuffer.allocate(Long.SIZE).putLong(l).array());
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }

    @Override
    public Long read(InputStream in) throws IOException {
        final byte[] bytes = new byte[Long.SIZE];
        in.read(bytes);
        return ByteBuffer.wrap(bytes).getLong();
    }


}
