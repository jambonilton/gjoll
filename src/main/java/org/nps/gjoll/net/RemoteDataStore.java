package org.nps.gjoll.net;

import org.nps.gjoll.Counts;
import org.nps.gjoll.DTO;
import org.nps.gjoll.DataStore;
import org.nps.gjoll.events.Change;
import org.nps.gjoll.events.StandardChangeFormat;
import org.nps.gjoll.io.Deserializer;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.Serializer;
import org.nps.gjoll.net.requests.*;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.retry.Retry;
import org.nps.gjoll.updates.Update;

import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class RemoteDataStore implements DataStore {

    final String name;
    final String host;
    final int port;
    final Serializer<DataStoreRequest> requestSerializer;
    final Deserializer<Counts> countsDeserializer;
    final Format<DTO> format;
    final Format<Change> changeFormat;
    final ExecutorService eventThreads;

    public RemoteDataStore(String name,
                           String host,
                           int port,
                           Format<DTO> format,
                           Format<Query> queryFormat,
                           Format<Update> updateFormat) {
        this(name, host, port, new DataStoreRequestFormat(format, queryFormat, updateFormat), new CountsDeserializer(), format, new StandardChangeFormat());
    }

    public RemoteDataStore(String name, String host, int port,
            Serializer<DataStoreRequest> requestSerializer,
            Deserializer<Counts> countsDeserializer,
            Format<DTO> serialStreamer,
            Format<Change> changeFormat) {
        this.name = name;
        this.host = host;
        this.port = port;
        this.requestSerializer = requestSerializer;
        this.countsDeserializer = countsDeserializer;
        this.format = serialStreamer;
        this.changeFormat = changeFormat;
        this.eventThreads = Executors.newCachedThreadPool();
    }

    @Override
    public Stream<DTO> get() {
        return get("");
    }

    @Override
    public Stream<DTO> get(Query query) {
        return stream(new DataStoreQueryRequest(name, query), format);
    }

    @Override
    public void add(DTO item) {
        add(Stream.of(item));
    }

    @Override
    public void add(Stream<? extends DTO> stream) {
        callRemotely(new DataStoreInsertRequest(name, stream));
    }

    @Override
    public Counts counts(Query query, String groupBy) {
        return callRemotely(new DataStoreCountsRequest(name, groupBy, query), countsDeserializer);
    }

    @Override
    public long size() {
        return callRemotely(new DataStoreSizeRequest(name), new LongFormat());
    }

    @Override
    public void apply(Update update) {
        callRemotely(new DataStoreUpdateRequest(name, update));
    }

    /**
     * Creates a persistent socket with the server which is used to pass up change events.
     * TODO should use one socket per client instead of one per subscriber
     */
    @Override
    public void onChange(Query criteria, Consumer<Change> action) {
        eventThreads.execute(()->stream(new DataStoreSubscribeRequest(name, criteria), changeFormat).forEach(action));
    }

    private void callRemotely(DataStoreRequest request) {
        callRemotely(request, NO_OP);
    }
    
    static final Deserializer<Boolean> NO_OP = in -> true;
    
    private <T> T callRemotely(DataStoreRequest request, Deserializer<T> deserializer) {
        try {
            return Retry.wrap(()->doRemoteCall(request, deserializer)).times(50).delay(300L).on(ConnectException.class, SocketException.class).call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private <T> T doRemoteCall(DataStoreRequest request, Deserializer<T> deserializer) throws Exception {
        try (Socket socket = getSocket()) {
            requestSerializer.write(request, socket.getOutputStream());
            return deserializer.read(socket.getInputStream());
        }
    }
    
    private <T> Stream<T> stream(DataStoreRequest request, Deserializer<T> deserializer) {
        try {
            Socket socket = getSocket();
            requestSerializer.write(request, socket.getOutputStream());
            return deserializer.stream(socket.getInputStream());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Socket getSocket() throws Exception {
        return Retry.wrap(()->new Socket(host,port)).times(50).delay(300L).on(ConnectException.class).call();
    }

}
