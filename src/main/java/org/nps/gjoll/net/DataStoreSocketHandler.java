package org.nps.gjoll.net;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.function.Consumer;

import org.nps.gjoll.Counts;
import org.nps.gjoll.DataStore;
import org.nps.gjoll.DataStoreRegistry;
import org.nps.gjoll.DTO;
import org.nps.gjoll.events.Change;
import org.nps.gjoll.io.Deserializer;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.Serializer;
import org.nps.gjoll.net.requests.DataStoreCountsRequest;
import org.nps.gjoll.net.requests.DataStoreInsertRequest;
import org.nps.gjoll.net.requests.DataStoreMethod;
import org.nps.gjoll.net.requests.DataStoreQueryRequest;
import org.nps.gjoll.net.requests.DataStoreRequest;
import org.nps.gjoll.net.requests.DataStoreRequestFormat;
import org.nps.gjoll.net.requests.DataStoreSubscribeRequest;
import org.nps.gjoll.net.requests.DataStoreUpdateRequest;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.updates.Update;

public class DataStoreSocketHandler implements SocketConsumer {

    final Deserializer<DataStoreRequest> deserializer;
    final Serializer<Counts> countsSerializer;
    final DataStoreRegistry dataStores;
    final Format<DTO> format;
    final Format<Change> changeFormat;

    public DataStoreSocketHandler(DataStoreRegistry dataStores,
                                  Format<DTO> format,
                                  Format<Query> queryFormat,
                                  Format<Update> updateFormat,
                                  Format<Change> changeFormat) {
        this(new DataStoreRequestFormat(format, queryFormat,
                updateFormat), new CountsSerializer(), dataStores, format,
                changeFormat);
    }

    public DataStoreSocketHandler(Deserializer<DataStoreRequest> deserializer,
                                  Serializer<Counts> countsSerializer, DataStoreRegistry dataStores,
                                  Format<DTO> format, Format<Change> changeFormat) {
        this.deserializer = deserializer;
        this.countsSerializer = countsSerializer;
        this.dataStores = dataStores;
        this.format = format;
        this.changeFormat = changeFormat;
    }

    @Override
    public void accept(Socket socket) throws IOException {
        final DataStoreRequest request = deserializer
                .read(socket.getInputStream());
        final DataStoreMethod method = request.getMethod();
        final DataStore dataStore = dataStores.get(request.getName());

        switch (method) {
        case AGG:
            final DataStoreCountsRequest countsRequest = (DataStoreCountsRequest) request;
            final Counts counts = dataStore.counts(countsRequest.getQuery(), countsRequest.getCountBy());
            countsSerializer.write(counts, socket.getOutputStream());
            break;
        case GET:
            format.write(dataStore.get(((DataStoreQueryRequest) request).getQuery()), socket.getOutputStream());
            break;
        case ADD:
            dataStore.add(((DataStoreInsertRequest) request).getItems());
            break;
        case PUT:
            dataStore.apply(((DataStoreUpdateRequest) request).getUpdate());
            break;
        case SUB:
            dataStore.onChange(((DataStoreSubscribeRequest) request).getQuery(), changeSocketConsumer(socket.getOutputStream()));
            break;
        case SIZE:
            LongFormat.get().write(dataStore.size(), socket.getOutputStream());
            break;
        }
    }

    // TODO force the socket to stay open.
    private Consumer<Change> changeSocketConsumer(final OutputStream out) {
        return (c) -> changeFormat.write(c, out);
    }

}
