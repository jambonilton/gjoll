package org.nps.gjoll.net;

import java.io.IOException;
import java.net.Socket;

public interface SocketConsumer {
    void accept(Socket socket) throws IOException;
}
