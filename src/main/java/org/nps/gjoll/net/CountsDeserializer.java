package org.nps.gjoll.net;

import java.io.IOException;
import java.io.InputStream;

import org.nps.gjoll.Counts;
import org.nps.gjoll.io.Deserializer;
import org.nps.gjoll.std.MapCounts;

public class CountsDeserializer implements Deserializer<Counts> {

    @Override
    public Counts read(InputStream in) throws IOException {
        MapCounts counts = new MapCounts();
        String line = readLine(in);
        while (!line.isEmpty()) {
            final String[] entry = line.split(",");
            counts.put(entry[0], Long.parseLong(entry[1]));
            line = readLine(in);
        }
        return counts;
    }

    private static String readLine(InputStream in) throws IOException {
        final StringBuilder sb = new StringBuilder();
        int c = 0;
        while (!isEndOfLine(c = in.read()))
            sb.append((char) c);
        return sb.toString();
    }
    
    private static boolean isEndOfLine(int i) {
        return i=='\n' || i==-1;
    }
    
}
