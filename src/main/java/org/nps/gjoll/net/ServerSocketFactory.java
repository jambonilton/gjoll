package org.nps.gjoll.net;

import java.net.ServerSocket;

public interface ServerSocketFactory {

    ServerSocket make();
    
}
