package org.nps.gjoll.net.requests;

import org.nps.gjoll.query.Query;

public class DataStoreCountsRequest extends DataStoreQueryRequest {

    final String countBy;
    
    public DataStoreCountsRequest(String name, String countBy, Query query) {
        super(DataStoreMethod.AGG, name, query);
        this.countBy = countBy;
    }

    public String getCountBy() {
        return countBy;
    }

}
