package org.nps.gjoll.net.requests;

import org.nps.gjoll.DTO;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.updates.Update;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static org.nps.gjoll.io.IOUtils.readLine;

public class DataStoreRequestFormat implements Format<DataStoreRequest> {

    final Format<DTO> format;
    final Format<Query> queryFormat;
    final Format<Update> updateFormat;

    public DataStoreRequestFormat(Format<DTO> format,
                                  Format<Query> queryFormat, 
                                  Format<Update> updateFormat) {
        this.format = format;
        this.queryFormat = queryFormat;
        this.updateFormat = updateFormat;
    }

    @Override
    public void write(DataStoreRequest request, OutputStream out) {
        try {
            final DataStoreMethod method = request.getMethod();
            out.write((method.name()+' '+request.getName()+'\n').getBytes());
            
            switch (method) {
            case AGG:
                final DataStoreCountsRequest countsQuery = (DataStoreCountsRequest) request;
                out.write(countsQuery.getCountBy().getBytes());
                out.write('\n');
                out.write(countsQuery.getQuery().toString().getBytes());
                out.write('\n');
                break;
            case GET:
                out.write(((DataStoreQueryRequest) request).getQuery().toString().getBytes());
                break;
            case ADD:
                format.write(((DataStoreInsertRequest) request).getItems(), out);
                break;
            case PUT:
                updateFormat.write(((DataStoreUpdateRequest) request).getUpdate(), out);
                break;
            case SUB:
                queryFormat.write(((DataStoreSubscribeRequest) request).getQuery(), out);
                break;
            case SIZE:
                break;
            }
            out.write('\n');
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public DataStoreRequest read(InputStream in) throws IOException {
        final RequestHeader header = getHeader(in);
        final DataStoreMethod method = header.getMethod();
        final String name = header.getName();
        switch (method) {
        case GET:
            return new DataStoreQueryRequest(name, queryFormat.read(readLine(in)));
        case AGG:
            return new DataStoreCountsRequest(name, readLine(in), queryFormat.read(readLine(in)));
        case ADD:
            return new DataStoreInsertRequest(name, format.stream(in));
        case SUB:
            return new DataStoreSubscribeRequest(name, queryFormat.read(in));
        case SIZE:
            return new DataStoreSizeRequest(name);
        case PUT:
            return new DataStoreUpdateRequest(name, updateFormat.read(in));
        }
        throw new IllegalArgumentException("Unknown method \""+method+"\"");
    }
    
    private static RequestHeader getHeader(InputStream in) throws IOException {
        final String header = readLine(in);
        final int space = header.indexOf(' ');
        if (space < 0)
            throw new InvalidRequestException(String.format("Invalid header \"%s\".", header));
        final DataStoreMethod method = DataStoreMethod.valueOf(header.substring(0, space));
        final String name = header.substring(space + 1);

        return new RequestHeader(method, name);
    }

    private static class RequestHeader {
        final DataStoreMethod method;
        final String name;

        public RequestHeader(DataStoreMethod method, String name) {
            this.method = method;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public DataStoreMethod getMethod() {
            return method;
        }
    }

    public static class InvalidRequestException extends RuntimeException {
        public InvalidRequestException(String message) {
            super(message);
        }
    }
}
