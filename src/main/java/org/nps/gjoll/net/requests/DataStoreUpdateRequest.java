package org.nps.gjoll.net.requests;

import org.nps.gjoll.updates.Update;

public class DataStoreUpdateRequest extends DataStoreRequest {

    final Update update;
    
    public DataStoreUpdateRequest(String name, Update update) {
        super(DataStoreMethod.PUT, name);
        this.update = update;
    }

    public Update getUpdate() {
        return update;
    }

}
