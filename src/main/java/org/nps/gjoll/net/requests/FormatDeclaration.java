package org.nps.gjoll.net.requests;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.QueryableFormat;

public class FormatDeclaration implements Format<QueryableFormat> {

    enum FormatTypes {
        JSON, // json
        CSV, // comma-separated values
        JOS  // java object serialization
    }
    
    @Override
    public void write(QueryableFormat format, OutputStream out) {
    }

    @Override
    public QueryableFormat read(InputStream in) throws IOException {
        return null;
    }

}
