package org.nps.gjoll.net.requests;

import org.nps.gjoll.query.Query;

public class DataStoreSubscribeRequest extends DataStoreRequest {

    final Query query;
    
    public DataStoreSubscribeRequest(String name, Query query) {
        super(DataStoreMethod.SUB, name);
        this.query = query;
    }

    public DataStoreSubscribeRequest(DataStoreMethod method, String name, Query query) {
        super(method, name);
        this.query = query;
    }

    public Query getQuery() {
        return query;
    }
}
