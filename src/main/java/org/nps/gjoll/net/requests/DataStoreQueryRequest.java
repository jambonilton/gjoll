package org.nps.gjoll.net.requests;

import org.nps.gjoll.query.Query;

public class DataStoreQueryRequest extends DataStoreRequest {

    final Query query;
    
    public DataStoreQueryRequest(String name, Query query) {
        super(DataStoreMethod.GET, name);
        this.query = query;
    }

    public DataStoreQueryRequest(DataStoreMethod method, String name, Query query) {
        super(method, name);
        this.query = query;
    }

    public Query getQuery() {
        return query;
    }

    @Override
    public int hashCode() {
        return query.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof DataStoreQueryRequest 
                && query.equals(((DataStoreQueryRequest) obj).getQuery());
    }

}
