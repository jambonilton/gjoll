package org.nps.gjoll.net.requests;

public class DataStoreSizeRequest extends DataStoreRequest {

    public DataStoreSizeRequest(String name) {
        super(DataStoreMethod.SIZE, name);
    }

}
