package org.nps.gjoll.net.requests;

public enum DataStoreMethod {
    GET, 
    ADD, 
    AGG, 
    SIZE, 
    PUT, 
    SUB
}