package org.nps.gjoll.net.requests;

import org.nps.gjoll.DTO;

import java.util.stream.Stream;

public class DataStoreInsertRequest extends DataStoreRequest {

    final Stream<? extends DTO> items;
    
    public DataStoreInsertRequest(String name, Stream<? extends DTO> items) {
        super(DataStoreMethod.ADD, name);
        this.items = items;
    }

    public Stream<? extends DTO> getItems() {
        return items;
    }
    
}
