package org.nps.gjoll.net.requests;

import org.nps.gjoll.DataStoreRegistry;
import org.nps.gjoll.Gjoll;
import org.nps.gjoll.io.format.JsonFormat;
import org.nps.gjoll.net.DataStoreSocketHandler;
import org.nps.gjoll.net.ServerThread;

import java.util.concurrent.Executors;

public class DataStoreServerRunner {

    public static void runServer(DataStoreRegistry stores, int port) {
        Executors.newSingleThreadExecutor().execute(new ServerThread(Executors.newCachedThreadPool(), port, new DataStoreSocketHandler(stores, new JsonFormat(), Gjoll.getQueryFormat(), Gjoll.getUpdateFormat(), Gjoll.getChangeFormat())));
    }

}
