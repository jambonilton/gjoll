package org.nps.gjoll.net.requests;

public abstract class DataStoreRequest {
    
    final DataStoreMethod method;
    final String name;
    
    public DataStoreRequest(DataStoreMethod method, String name) {
        this.method = method;
        this.name = name;
    }

    public DataStoreMethod getMethod() {
        return method;
    }

    public String getName() {
        return name;
    }
}
