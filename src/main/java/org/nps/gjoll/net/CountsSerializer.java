package org.nps.gjoll.net;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.nps.gjoll.Counts;
import org.nps.gjoll.io.Serializer;
import org.nps.gjoll.query.Pair;

public class CountsSerializer implements Serializer<Counts> {

    @Override
    public void write(Counts counts, OutputStream out) {
        try {
            final Writer writer = new OutputStreamWriter(out);
            for (final Pair<Object,Long> entry : counts)
                writer.write(entry.getKey()+","+entry.getValue()+"\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
