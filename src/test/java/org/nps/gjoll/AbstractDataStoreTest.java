package org.nps.gjoll;

import org.junit.Before;
import org.junit.Test;
import org.nps.gjoll.io.format.JsonFormat;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.nps.gjoll.query.Query.greaterThan;
import static org.nps.gjoll.query.Query.is;

public abstract class AbstractDataStoreTest<D extends ReadOnlyStore> {

    protected final D store;

    public AbstractDataStoreTest(D source) {
        this.store = source;
    }

    @Before
    public void setUp() throws Exception {
        addUsers(store);
    }

    protected static void addUsers(ReadOnlyStore store) throws IOException {
        store.add(new JsonFormat().stream(AbstractDataStoreTest.class
                .getResourceAsStream("/users.json")));
    }

    @Test
    public void get() {
        assertEquals(6, store.get().count());
        assertEquals(6, store.size());
    }

    @Test
    public void getWithBuilder() {
        assertEquals(4, store.get(is("sex", "M")).count());
        assertEquals(2, store.get(is("sex", "M").and(is("occupation", "Programmer"))).count());
        assertEquals(1, store.get(is("sex", "M").and(is("occupation", "Programmer")).and(greaterThan("age", 35))).count());
    }

    @Test
    public void emptyQuery() {
        assertEquals(6, store.get("").count());
    }

    @Test
    public void emailContains() {
        assertEquals(3, store.get("email << 'gmail'").count());
    }

    @Test
    public void emailContainsAndIdInRange() {
        assertEquals(2, store.get("email << 'gmail' & id >= 3 & id < 5").count());
    }

    @Test
    public void sexIsIn() {
        assertEquals(6, store.get("sex>>'FM'").count());
    }

    @Test
    public void idGreaterThan() {
        assertEquals(4, store.get("id >= 3").count());
    }

    @Test
    public void sexAndOccupation() {
        assertEquals(1, store.get("sex = 'M' & occupation = 'Programmer' & age > 35").count());
    }

    @Test
    public void idRanges() {
        assertEquals(2, store.get("id >= 3 & id < 5").count());
        assertEquals(2, store.get("id > 3 & id <= 5").count());
        assertEquals(4, store.get("id <= 3 | id > 5").count());
        assertEquals(4, store.get("id < 3 | id >= 5").count());
    }

    @Test
    public void counts() {
        final Counts counts = store.counts("occupation = 'Programmer'", "sex");
        assertEquals(2L, (long) counts.get("M"));
        assertEquals(1L, (long) counts.get("F"));
    }

    @Test
    public void converts() {
        TestUser steve = new TestUser();
        steve.id = 1;
        steve.name = "Steve Camuti";
        steve.email = "scamuti@gmail.com";
        steve.age = 34;
        steve.sex = "M";
        steve.occupation = "Programmer";
        assertEquals(steve, store.get("id = 1").findFirst().get().asA(TestUser.class));
    }

    public static class TestUser {

        int id;
        String name;
        String email;
        int age;
        String sex;
        String occupation;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TestUser testUser = (TestUser) o;

            if (id != testUser.id) return false;
            if (age != testUser.age) return false;
            if (!name.equals(testUser.name)) return false;
            if (!email.equals(testUser.email)) return false;
            if (!sex.equals(testUser.sex)) return false;
            return occupation.equals(testUser.occupation);

        }

        @Override
        public int hashCode() {
            int result = id;
            result = 31 * result + name.hashCode();
            result = 31 * result + email.hashCode();
            result = 31 * result + age;
            result = 31 * result + sex.hashCode();
            result = 31 * result + occupation.hashCode();
            return result;
        }
    }

}