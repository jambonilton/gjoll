package org.nps.gjoll.tables;

import static org.nps.gjoll.tables.LiteralType.INT;
import static org.nps.gjoll.tables.LiteralType.STRING;

import java.io.File;
import java.io.IOException;

import org.nps.gjoll.AbstractDataStoreTest;
import org.nps.gjoll.std.FileStore;

public class TableFormatFileStoreTest extends AbstractDataStoreTest<FileStore> {

    public TableFormatFileStoreTest() {
        super(makeFileStore());
    }

    private static FileStore makeFileStore() {
        try {
            final File file = File.createTempFile("test", "data");
            file.createNewFile();
            final TableDescriptor table = new TableDescriptor.Builder()
                    .column("id", INT, 4)
                    .column("name", STRING, 128)
                    .column("email", STRING, 128)
                    .column("age", INT, 2)
                    .column("sex", STRING, 1)
                    .column("occupation", STRING, 64)
                    .build();
            return new FileStore(file, new TableFormat(table));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
}
