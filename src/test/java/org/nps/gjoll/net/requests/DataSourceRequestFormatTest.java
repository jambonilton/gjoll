package org.nps.gjoll.net.requests;

import org.junit.Test;
import org.nps.gjoll.DTO;
import org.nps.gjoll.io.format.JsonFormat;
import org.nps.gjoll.io.format.ListDTO;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.StandardQueryFormat;
import org.nps.gjoll.updates.StandardUpdateFormat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.nps.gjoll.net.requests.DataStoreMethod.*;
import static org.nps.gjoll.query.Query.*;

public class DataSourceRequestFormatTest {

    final DataStoreRequestFormat format = new DataStoreRequestFormat(
            new JsonFormat(), new StandardQueryFormat(), new StandardUpdateFormat());
    final String name = "test";

    @Test
    public void deserializeQuery() throws IOException {
        final InputStream in = new ByteArrayInputStream((GET+" "+name+"\nid=1").getBytes());
        final DataStoreRequest request = format.read(in);
        assertTrue(request instanceof DataStoreQueryRequest);
        final DataStoreQueryRequest query = (DataStoreQueryRequest) request;
        assertEquals("id=1", query.getQuery().toString());
    }

    @Test
    public void deserializeAddFromFile() throws IOException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write((ADD+" "+name+"\n").getBytes());
        final InputStream fileIn = this.getClass().getResourceAsStream("/users.json");
        final byte[] buff = new byte[256];
        int read = 0;
        while ((read = fileIn.read(buff)) != -1)
            out.write(buff, 0, read);
        final DataStoreRequest request = format.read(new ByteArrayInputStream(out.toByteArray()));
        assertTrue(request instanceof DataStoreInsertRequest);
        final DataStoreInsertRequest insert = (DataStoreInsertRequest) request;
        assertEquals(6L, insert.getItems().count());
    }
    
    @Test
    public void deserializeUpdate() throws IOException {
        final InputStream in = new ByteArrayInputStream((PUT+" "+name+"\nid←1,foo←'bar'\nid=1&foo='bar'").getBytes());
        final DataStoreRequest request = format.read(in);
        assertTrue(request instanceof DataStoreUpdateRequest);
        final DataStoreUpdateRequest update = (DataStoreUpdateRequest) request;
        final DTO obj = new ListDTO(update.getUpdate().assignments(null));
        assertEquals(1, obj.get("id"));
        assertEquals("bar", obj.get("foo"));
        assertEquals("id=1 & foo=\"bar\"", update.getUpdate().where().toString());
    }
    
    @Test
    public void deserializeSubscription() throws IOException {
        final InputStream in = new ByteArrayInputStream((SUB+" "+name+"\nid=1").getBytes());
        final DataStoreRequest request = format.read(in);
        assertTrue(request instanceof DataStoreSubscribeRequest);
        final DataStoreSubscribeRequest query = (DataStoreSubscribeRequest) request;
        assertEquals("id=1", query.getQuery().toString());
    }
    
    @Test
    public void deserializeCounts() throws IOException {
        final InputStream in = new ByteArrayInputStream((AGG+" "+name+"\noccupation\nid=1").getBytes());
        final DataStoreRequest request = format.read(in);
        assertTrue(request instanceof DataStoreCountsRequest);
        final DataStoreCountsRequest query = (DataStoreCountsRequest) request;
        assertEquals("id=1", query.getQuery().toString());
        assertEquals("occupation", query.getCountBy());
    }
    
    @Test
    public void deserializeSize() throws IOException {
        final InputStream in = new ByteArrayInputStream((SIZE.name()+" "+name).getBytes());
        final DataStoreRequest request = format.read(in);
        assertTrue(request instanceof DataStoreSizeRequest);
    }
    
    @Test
    public void roundTripQuery() {
        final Query query = and(is("id", 1), greaterThan("age", 30));
        final DataStoreQueryRequest request = new DataStoreQueryRequest(name, query);
        assertEquals(request, format.read(format.toString(request)));
    }

    // TODO more tests.
    
}
