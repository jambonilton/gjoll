package org.nps.gjoll.net;

import org.junit.BeforeClass;
import org.nps.gjoll.AbstractDataStoreTest;
import org.nps.gjoll.DataStore;
import org.nps.gjoll.DataStoreRegistry;
import org.nps.gjoll.DTO;
import org.nps.gjoll.events.DataPublishingStore;
import org.nps.gjoll.events.StandardChangeFormat;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.format.JsonFormat;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.StandardQueryFormat;
import org.nps.gjoll.std.ListStore;
import org.nps.gjoll.updates.StandardUpdateFormat;
import org.nps.gjoll.updates.Update;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class RemoteDataStoreTest extends AbstractDataStoreTest<RemoteDataStore> {

    private static final int PORT = 18092;
    private static final Format<DTO> FORMAT = new JsonFormat();
    private static final Format<Query> QUERY_FORMAT = new StandardQueryFormat();
    private static final Format<Update> UPDATE_FORMAT = new StandardUpdateFormat();
    private static final String NAME = "test";

    @BeforeClass
    public static void startServer() throws Exception {
        Executor exec = Executors.newCachedThreadPool();
        final DataStore bucket = new DataPublishingStore<>(new ListStore());
        addUsers(bucket);
        final DataStoreRegistry registry = DataStoreRegistry.of(NAME, bucket);
        exec.execute(new ServerThread(exec, PORT, new DataStoreSocketHandler(registry,
                new JsonFormat(), new StandardQueryFormat(), new StandardUpdateFormat(), new StandardChangeFormat())));
    }
    
    @Override
    public void setUp() throws Exception {
        // already populated
    }
    
    public RemoteDataStoreTest() {
        super(new RemoteDataStore(NAME, "localhost", PORT, FORMAT, QUERY_FORMAT, UPDATE_FORMAT));
    }
    
}
