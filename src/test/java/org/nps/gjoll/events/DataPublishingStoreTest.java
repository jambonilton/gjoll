package org.nps.gjoll.events;

import org.nps.gjoll.AbstractDataStoreTest;
import org.nps.gjoll.std.ListStore;

public class DataPublishingStoreTest extends AbstractDataStoreTest<DataPublishingStore<ListStore>> {

    public DataPublishingStoreTest() {
        super(new DataPublishingStore<ListStore>(new ListStore()));
    }
    
}
