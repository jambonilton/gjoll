package org.nps.gjoll.std;

import org.nps.gjoll.AbstractDataStoreTest;
import org.nps.gjoll.io.format.JsonFormat;

import java.io.File;
import java.io.IOException;

public class JsonFileStoreTest extends AbstractDataStoreTest<FileStore> {

    public JsonFileStoreTest() {
        super(makeFileStore());
    }

    private static FileStore makeFileStore() {
        try {
            final File file = File.createTempFile("test", "json");
            file.deleteOnExit();
            return new FileStore(file, new JsonFormat());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
