package org.nps.gjoll.std;

import org.junit.Ignore;
import org.nps.gjoll.AbstractDataStoreTest;
import org.nps.gjoll.index.ListLongIndex;
import org.nps.gjoll.index.ranges.LongBiIndexImpl;
import org.nps.gjoll.io.format.BsonFormat;

import java.io.File;
import java.io.IOException;

@Ignore // I can't even.
public class BsonFileStoreTest extends AbstractDataStoreTest<FileStore> {

    public BsonFileStoreTest() {
        super(makeFileStore());
    }

    private static FileStore makeFileStore() {
        try {
            final File file = File.createTempFile("test", "bson");
            file.deleteOnExit();
            return new FileStore(file, new BsonFormat(new LongBiIndexImpl<>(new ListLongIndex<>())));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
