package org.nps.gjoll.std;

import java.util.Arrays;

import org.nps.gjoll.AbstractDataStoreTest;

public class MultiDataStoreTest extends AbstractDataStoreTest<MultiStore> {

    public MultiDataStoreTest() {
        super(new MultiStore(Arrays.asList(new ListStore(), new ListStore(), new ListStore())));
    }
    
}
