package org.nps.gjoll.std;

import org.nps.gjoll.AbstractDataStoreTest;

public class ListStoreTest extends AbstractDataStoreTest<ListStore> {

    public ListStoreTest() {
        super(new ListStore());
    }
    
}
