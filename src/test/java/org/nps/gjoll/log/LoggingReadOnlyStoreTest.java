package org.nps.gjoll.log;

import org.nps.gjoll.AbstractDataStoreTest;
import org.nps.gjoll.io.format.JsonFormat;
import org.nps.gjoll.query.StandardQueryFormat;
import org.nps.gjoll.std.ListStore;

public class LoggingReadOnlyStoreTest extends AbstractDataStoreTest<LoggingReadOnlyStore<ListStore>> {

    public LoggingReadOnlyStoreTest() {
        super(new LoggingReadOnlyStore<>(new ListStore(), new PrintStreamLogWriter("TEST"), new JsonFormat(), new StandardQueryFormat()));
    }
    
}
