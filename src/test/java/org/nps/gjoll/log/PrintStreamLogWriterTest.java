package org.nps.gjoll.log;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import org.junit.Before;
import org.junit.Test;

// TODO test is very tightly coupled to implementation

public class PrintStreamLogWriterTest {

    private static final String NAME = "TEST", MSG = "FEST";
    
    ByteArrayOutputStream out;
    Clock clock;
    
    PrintStreamLogWriter log;
    
    @Before
    public void setUp() {
        out = new ByteArrayOutputStream();
        final PrintStream printStream = new PrintStream(out);
        clock = Clock.fixed(Instant.now(), ZoneId.systemDefault());
        log = new PrintStreamLogWriter(NAME, printStream, clock);
    }
    
    @Test
    public void testInfo() {
        log.info(MSG);
        assertMessageLogged("INFO");
    }
    @Test
    public void testWarn() {
        log.warn(MSG);
        assertMessageLogged("WARN");
    }
    @Test
    public void testWarnStackTrace() {
        Throwable e = new NullPointerException();
        log.warn(MSG, e);
        assertStackTraceLogged("WARN", e);
    }
    @Test
    public void testErrorStackTrace() {
        Throwable e = new StackOverflowError();
        log.error(MSG, e);
        assertStackTraceLogged("ERROR", e);
    }

    private void assertMessageLogged(final String level) {
        assertEquals(clock.instant()+" "+level+" "+NAME+" - "+MSG+System.lineSeparator(), out.toString());
    }

    private void assertStackTraceLogged(final String level, final Throwable t) {
        final String[] lines = out.toString().split(System.lineSeparator());
        assertEquals(clock.instant()+" "+level+" "+NAME+" - "+MSG, lines[0]);
        assertEquals("    "+t.getClass().getName()+": "+t.getMessage(), lines[1]);
        for (int i=0; i < t.getStackTrace().length; i++) {
            StackTraceElement elem = t.getStackTrace()[i];
            assertEquals("        "+elem.getClassName()+'.'+elem.getMethodName()+":"+elem.getLineNumber(), lines[i+2]);
        }
    }
    
}
