package org.nps.gjoll.retry;

import static org.junit.Assert.assertEquals;

import java.net.SocketException;
import java.util.concurrent.Callable;

import org.junit.Test;

public class RetryTest {

	private static final String RESPONSE = "test";
	private static final Exception ex = new SocketException("er mah gerd");
	
	@Test
	public void constantDelayRetry() throws Exception {
		assertEquals(RESPONSE, Retry.wrap(new FailTask(ex, 3)).times(4).delay(1).call());
	}
	
	@Test
	public void serialDelayRetry() throws Exception {
		assertEquals(RESPONSE, Retry.wrap(new FailTask(ex, 3)).delays(1,2,3,4).call());
		assertEquals(RESPONSE, Retry.wrap(new FailTask(ex, 3)).delays(1,2).times(4).call());
	}
	
	@Test(expected = SocketException.class)
	public void failsWhenRetriesExhausted() throws Exception {
		assertEquals(RESPONSE, Retry.wrap(new FailTask(ex, 3)).delays(1,2).call());
	}
	
	static class FailTask implements Callable<String> {

		final Exception ex;
		final int times;
		
		int i=0;
				
		public FailTask(Exception ex, int times) {
			this.ex = ex;
			this.times = times;
		}

		@Override
		public String call() throws Exception {
			if (i++ < times)
				throw ex;
			return RESPONSE;
		}
		
	}
	
}
