package org.nps.gjoll.query;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.format.BadCharacterException;

public class StandardQueryFormatTest {

    Format<Query> format = new StandardQueryFormat();
    
    @Test
    public void eq() {
        assertQueryParses("a=1");
    }
    @Test
    public void lt() {
        assertQueryParses("a<1");
    }
    @Test
    public void gt() {
        assertQueryParses("a>1");
    }
    @Test
    public void leq() {
        assertQueryParses("a<=1");
    }
    @Test
    public void geq() {
        assertQueryParses("a>=1");
    }
    @Test
    public void whitespace() {
        assertQueryParses("a = 1", "a=1");
    }
    @Test
    public void stringValue() {
        assertQueryParses("a='test'", "a=\"test\"");
        assertQueryParses("a=\"test\"");
    }
    @Test
    public void arrayContains() {
        assertQueryParses("a>>[1,2,3]");
    }
    @Test(expected = BadCharacterException.class)
    public void badJson() {
        assertQueryParses("a>>[1,2,3");
    }
    @Test
    public void and() {
        assertQueryParses("a=1 & b=2 & c=3");
    }
    @Test
    public void or() {
        assertQueryParses("a=1 | b=2 | c=3");
    }
    @Test(expected = NormalFormRequiredException.class)
    public void abnormal() {
        assertQueryParses("a=1 & b=2 | c=3");
    }
    @Test
    public void sub() {
        assertQueryParses("a=1 & (b<=2 | c>=3)");
    }
    @Test(expected = BadCharacterException.class)
    public void unclosed() {
        assertQueryParses("a=1 & (b<=2 | c>=3");
    }
    private void assertQueryParses(final String query) {
        assertQueryParses(query, query);
    }

    private void assertQueryParses(final String toParse, final String expected) {
        assertEquals(expected, format.read(toParse).toString());
    }
    
}
