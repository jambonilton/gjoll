package org.nps.gjoll.index;

import org.nps.gjoll.AbstractDataStoreTest;
import org.nps.gjoll.io.format.JsonFormat;
import org.nps.gjoll.std.FileStore;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class MultiIndexFileStoreTest extends AbstractDataStoreTest<IndexedDataStore> {

    public MultiIndexFileStoreTest() throws IOException {
        super(new IndexedDataStore(new MultiIndex(Arrays.<Index>asList(new TreeIndex<Long>("id"), new ContainsIndex("email", new RegexStringItemizer()))), new FileStore(File.createTempFile("test", "json"), new JsonFormat())));
    }
}
