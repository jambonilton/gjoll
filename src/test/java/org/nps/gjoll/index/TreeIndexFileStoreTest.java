package org.nps.gjoll.index;

import java.io.File;
import java.io.IOException;

import org.nps.gjoll.AbstractDataStoreTest;
import org.nps.gjoll.io.format.JsonFormat;
import org.nps.gjoll.std.FileStore;

public class TreeIndexFileStoreTest extends AbstractDataStoreTest<IndexedDataStore> {

    public TreeIndexFileStoreTest() throws IOException {
        super(new IndexedDataStore(new TreeIndex<Long>("id"), new FileStore(File.createTempFile("test", "json"), new JsonFormat())));
    }
    
}
