package org.nps.gjoll.index;

import java.io.File;
import java.io.IOException;

import org.nps.gjoll.AbstractDataStoreTest;
import org.nps.gjoll.io.format.JsonFormat;
import org.nps.gjoll.std.FileStore;

public class ContainsIndexListStoreTest extends AbstractDataStoreTest<IndexedDataStore> {

    public ContainsIndexListStoreTest() throws IOException {
        super(new IndexedDataStore(new ContainsIndex("email", new RegexStringItemizer()), new FileStore(File.createTempFile("test", "json"), new JsonFormat())));
    }
    
}
