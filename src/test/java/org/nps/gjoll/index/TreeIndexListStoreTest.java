package org.nps.gjoll.index;

import org.nps.gjoll.AbstractDataStoreTest;
import org.nps.gjoll.std.ListStore;

public class TreeIndexListStoreTest extends AbstractDataStoreTest<IndexedDataStore> {

    public TreeIndexListStoreTest() {
        super(new IndexedDataStore(new TreeIndex<Long>("id"), new ListStore()));
    }

}
