package org.nps.gjoll.streams;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class StreamMergingTest {
 
    private final List<Integer> expectedMerge;
    private final Stream<Integer> mergedStream;
 
    public StreamMergingTest(List<Integer> list1, List<Integer> list2, List<Integer> expected) {
        expectedMerge = expected;
        mergedStream = StreamUtils.merge(list1.stream(), list2.stream());
    }
 
    @Test
    public void testStream() throws Exception {
        List<Integer> merged = mergedStream.collect(Collectors.<Integer>toList());
        assertEquals(expectedMerge, merged);
    }
 
    @Parameters
    public static List<Object[]> data() {
        return MergedIteratorTest.data();
    }
}