package org.nps.gjoll.io.format;

import org.junit.Test;
import org.nps.gjoll.DTO;
import org.nps.gjoll.io.IOUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class JsonFormatTest {

    final JsonFormat format = new JsonFormat();
    
    @Test
    public void testParsing() {
        DTO object = format.read("{'field1':1, "
                                      + "'field2':true, "
                                      + "'field3':'three', "
                                      + "'field4':987654321"
                                      + "}");
        assertEquals(1, ((Number) object.get("field1")).intValue());
        assertEquals(true, object.get("field2"));
        assertEquals("three", object.get("field3"));
    }
    
    @Test
    public void testRoundTrip() {
        final String test = "{\"foo\":[\"bar\",1,false]}";
        assertEquals(test, format.toString(format.read(test)));
    }

    @Test
    public void testFile() throws IOException {
        final String test = IOUtils.read(JsonFormatTest.class.getResourceAsStream("/test.json")).replaceAll("\\s", "");
        assertEquals(test, format.toString(format.read(test)));
    }
    
}
