package org.nps.gjoll.io.format;

import org.junit.Test;
import org.nps.gjoll.DTO;
import org.nps.gjoll.index.ListLongIndex;
import org.nps.gjoll.index.ranges.LongBiIndexImpl;
import org.nps.gjoll.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class BsonFormatTest {

    private JsonFormat jsonFormat = new JsonFormat();
    private BsonFormat bsonFormat = new BsonFormat(new LongBiIndexImpl<>(new ListLongIndex<>()));

    @Test
    public void testJsonFile() throws IOException {
        final String test = IOUtils.read(JsonFormatTest.class.getResourceAsStream("/test.json")).replaceAll("\\s", "");
        System.out.println(test.getBytes().length + " bytes read.");
        final DTO obj = jsonFormat.read(test);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        bsonFormat.write(obj, out);
        final byte[] bytes = out.toByteArray();
        System.out.println(bytes.length + " bytes written.");
        final DTO fromBytes = bsonFormat.read(new ByteArrayInputStream(bytes));
        assertEquals(test, jsonFormat.toString(fromBytes));
    }

}