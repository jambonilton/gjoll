Gjöll
======

A stream-oriented database engine, tailored for Java applications.  The intent of the project is to provide a set of 
minimalist data communication interfaces for building highly-testable, loosely-coupled java applications.  The patterns 
used in this project lend themselves well to service-oriented or event-driven architecture.

## What's with the name?

The name is borrowed from the river which separates the living from the dead according to Norse Mythology.

## Getting Started

The objective with using Gjöll is to be able to quickly create persistent data stores.

Here's an example of how to create a simple in-memory data store with no indexing:

```
DataStore ds = Gjoll.newDataStore().build();
```

Or if you'd like to save your data to disk, you can use a file data store:

```
DataStore users = Gjoll.newDataStore("users").setDataFile("users.dat").build();
```

Or if you'd like to connect to a remote server:

```
DataStore events = Gjoll.newDataStore().connect("localhost", 8888).build();
```

Or run your own server:

```
Gjoll.broadcastOnPort(8888);
```

There is further documentation in the Gjoll and DataStoreBuilder class javadocs for creating and managing your data stores.

## Storing data

To add some data to a data store, simply call the *add()* method.  You'll note that the data store won't take any old 
Object as a parameter, so you'll have to implement the DTO interface.  Don't worry though, the required functionality 
is already built into the interface using reflection, so no further code is needed.

Here's an example object that you could send to your data store:

```
public class User implements DTO {
    public int id;
    public String name;
}
```

Now to send to the data store, just call *add()*.

```
User steve = new User();
steve.id = 1;
steve.name = "Steve";

dataStore.add(steve);
```

## Finding data

If you want to get your data back, call the *get()* method with whatever info you want to match on.

```
Stream<DTO> usersNamedSteve = dataStore.get("name = 'Steve'");
```

If you're not a fan of working with the generic *DTO* type, you can create a parameterized store.

```
Store<User> users = dataStore.forType(User.class);
User steve = dataStore.get(is("name","Steve")).findFirst().get();
```

## Query Language

Queries in Gjöll are normal-form sets of key-value comparisons.

For example,
```
name = 'Frank' & age >= 30
``` 
will return users with the name "Frank" and are older than 30.

Note that the fields in the query are on the left of the comparison, and the values are on the right.  The values are parsed as 
JSON format, so you can use any type of JSON values you wish, including arrays, strings, numbers, boolean, etc.

Because Gjöll expects normal-form queries, this would not be accepted due to mixing ANDs and ORs:
```
name = 'Frank' & age >= 30 | email << 'gmail'
```
It would need to be corrected like so:
```
(name = 'Frank' & age >= 30) | email << 'gmail'
```

#### Operators

The table below contains the operators that can be used for comparisons in your queries.

| operator | description     |
| -------- | --------------- |
| =  | equals                |
| <  | less than             |
| <= | less than or equal    |
| >  | greater than          |
| >= | greater than or equal |
| >> | is in collection      |
| << | contains element      |