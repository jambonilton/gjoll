# Gjöll-flavoured BSON

To store our NoSQL data in the smallest possible footprint, with the fastest possible read/write times, we use a 
minimized form of binary JSON.

Like JSON, our types include objects, integers, floats, string, booleans and nulls, but because they are encoded in 
byte code, we use finer-grained types to ensure our data is as small as possible on disc.

Below is the pseudo-[BNF](https://en.wikipedia.org/wiki/Backus%E2%80%93Naur_Form) notation to describe how it works:

```
object = size assignment*

assignment = key type value

key = byte byte

type = byte

value = object | array | string | integer | float | boolean | null

string = size byte*

```

Note, the "key" value is a two-byte address to the string which is indexed externally from the data.

Another important detail with the format is that the "size" value can be encoded as a byte, short, or int so that we 
can use as little space as possible.  All numbers are also stored this way.


### Limits

1. You can only have 32,767 different names for fields.  This is because they are all addressed with a 16-bit number.
2. You can only have 64-bit numbers.  We'll probably add support for really big numbers eventually.